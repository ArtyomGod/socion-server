﻿using System.Linq;
using System.Text;

namespace Model
{
	public class EmailInformation
	{
		public string FromName { get; set; }
		public string FromAddress { get; set; }
		public string ToName { get; set; }
		public string ToAddress { get; set; }
		public string Subject { get; set; }
		public string Body { get; set; }

		public override string ToString()
		{
			var result = new StringBuilder();
			result.AppendLine("From:");
			result.AppendLine($"\tName: {FromName}");
			result.AppendLine($"\tAddress: {FromAddress}");
			result.AppendLine("To:");
			result.AppendLine($"\tName: {ToName}");
			result.AppendLine($"\tAddress: {ToAddress}");
			result.AppendLine($"Subject: {Subject}");
			result.AppendLine($"Body: {Body}");

			return result.ToString();
		}
	}
}
