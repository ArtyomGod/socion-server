﻿using InstaSharper.Classes.Models;

namespace Model
{
	public class PaginationUserInfo
	{
		public PaginationUserInfo(InstaUser user, string startFromId, string usernames = null)
		{
			User = user;
			StartFromId = startFromId;
			Usernames = usernames;
		}
		public InstaUser User { get; }
		public string StartFromId { get; }
		public string Usernames { get; }
	}
}
