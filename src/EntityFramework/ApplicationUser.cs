﻿using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.AspNetCore.Identity;

namespace EntityFramework
{
	[Table("AspNetUsers")]
	public class ApplicationUser : IdentityUser<int>
	{
		[Column("email_new")]
		public string NewEmail { get; set; }
	}
}
