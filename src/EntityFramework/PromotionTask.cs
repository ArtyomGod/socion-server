﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using ConstantLibrary;

namespace EntityFramework
{
	[Table("promotion_task")]
	public class PromotionTask
	{
		[Column("id_promotion_task")]
		[Key]
		public uint PromotionTaskId { get; set; }

		[Column("status")]
		public int Status { get; set; }

		[Column("hash_tags")]
		public string HashTags { get; set; }

		[Column("days_to_delete_unsubscribed")]
		public int DaysToDelete { get; set; }

		[Column("type")]
		public PromotionTaskType PromotionTaskType { get; set; }

		[Column("competitors")]
		public string Competitors { get; set; }

		[Column("like_ammount")]
		public int? LikeAmmount { get; set; }

		[Column("locations")]
		public string Locations { get; set; }
	}
}
