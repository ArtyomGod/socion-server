﻿using System.ComponentModel.DataAnnotations.Schema;

namespace EntityFramework
{
	[Table("account_promotion_task")]
	public class AccountPromotionTask
	{
		[Column("id_promotion_task")]
		public uint TaskId { get; set; }

		[Column("id_social_network_account")]
		public int SocialNetworkAccountId { get; set; }
	}
}
