﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace EntityFramework
{
	[Table("social_network_account_proxy")]
	public class SocialNetworkAccountProxy
	{
		[Column("id_social_network_account")]
		[Key]
		public int SocialNetworkAccountId { get; set; }

		[Column("id_proxy")]
		public int ProxyId { get; set; }
	}
}
