﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace EntityFramework
{
	[Table("proxy")]
	public class Proxy
	{
		[Column("id_proxy")]
		public int ProxyId { get; set; }

		[Column("value")]
		public string Value { get; set; }

		[Column("is_valid")]
		public bool IsValid { get; set; }

		[Column("login")]
		public string Login { get; set; }

		[Column("password")]
		public string Password { get; set; }

		[Column("type")]
		public ConstantLibrary.ProxyType Type { get; set; }

		[Column("expiration_date")]
		public DateTime ExpirationDate { get; set; }
	}
}
