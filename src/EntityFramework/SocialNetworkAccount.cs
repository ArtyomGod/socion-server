﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace EntityFramework
{
	[Table("social_network_account")]
	public class SocialNetworkAccount
	{
		[Column("id_social_network_account")]
		public int SocialNetworkAccountId { get; set; }

		[Column("id_user")]
		public int UserId { get; set; }

		[Column("id_social_network")] 
		public int SocialNetworkId { get; set; }

		[Column("token")]
		public string AccessToken { get; set; }

		[Column("token_lifetime")]
		public DateTime? TokenLifetime { get; set; }

		[Column("login")]
		public string Login { get; set; }

		[Column("password")]
		public string Password { get; set; }
	}
}
