﻿using System.ComponentModel.DataAnnotations.Schema;

namespace EntityFramework
{
	[Table("user_proxy")]
	public class UserProxy
	{
		[Column("id_user")]
		public int UserId { get; set; }

		[Column("id_proxy")]
		public int ProxyId { get; set; }
	}
}
