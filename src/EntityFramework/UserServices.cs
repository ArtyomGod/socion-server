﻿using System.ComponentModel.DataAnnotations.Schema;

namespace EntityFramework
{
	[Table("user_services")]
	public class UserServices
	{
		[Column("id_user")]
		public int UserId { get; set; }

		[Column("id_service")]
		public int ServiceId { get; set; }
	}
}
