﻿using Microsoft.EntityFrameworkCore;

namespace EntityFramework
{
	public class SocionContext : DbContext
	{
		public SocionContext(DbContextOptions<SocionContext> options) : base(options)
		{

		}
		public SocionContext()
		{
		}

		protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
		{
			optionsBuilder.UseMySql(ConstantLibrary.Constant.ConnectionString.Socion);
		}

		protected override void OnModelCreating(ModelBuilder modelBuilder)
		{
			modelBuilder.Entity<UserServices>().HasKey(u => new { u.ServiceId, u.UserId});
			modelBuilder.Entity<AccountPromotionTask>().HasKey(a => new { a.TaskId, a.SocialNetworkAccountId});
			//modelBuilder.Entity<SocialNetworkAccountProxy>().HasKey(s => new { s.ProxyId, s.SocialNetworkAccountId});
			modelBuilder.Entity<UserProxy>().HasKey(s => new { s.UserId, s.ProxyId});
		}

		public DbSet<UserServices> UserServices { get; set; }
		public DbSet<SocialNetworkAccount> SocialNetworkAccounts { get; set; }
		public DbSet<PromotionTask> PromotionTasks { get; set; }
		public DbSet<AccountPromotionTask> AccountPromotionTasks { get; set; }
		public DbSet<Proxy> Proxies { get; set; }
		public DbSet<SocialNetworkAccountProxy> SocialNetworkAccountProxies { get; set; }
		public DbSet<UserProxy> UserProxies { get; set; }
	}
}
