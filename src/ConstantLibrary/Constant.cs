﻿namespace ConstantLibrary
{
	public enum ProxyType
	{
		Https = 0,
		Socks5
	}
	public enum SocialNetwork 
	{
		 Instagram = 0, 
		 Vk = 1,
		 Youtube = 2
	};

	public enum PromotionTaskType
	{//Добавлять новые типы всегда в конец !!!
		 SubscribeByTagInstagram = 0,
		 UnsubscribeInstagram,
		 SubscribeByCompetitor,
		 LikeFollowers,
		 LikeByTags,
		 SubscribeByLocationsInstagram,
		 LikeByLocationsInstagram
	};
	
	public static class Constant
	{
		public static class ProxyHelper
		{
			public const string AddresToCheckProxyResponseStatus = "https://www.google.com";
		}

		public static class SocialNetworkConstraints
		{
			public static int MaxAmmountToLikePerDayInst = 950; //1000
			public static int MaxAmmountToSubscribePerDayInst = 950; //1000;  per hour 200
			public static int MaxAmmountToUnsubscribePerDayInst = 950; //1000;  per hour 200
		}

		public static class InstagramApiHelper
		{
			public const int DefaultPageToLoadForPagination = 1;
			public const string StateFileName = "inst_";
			public const string FileType = "bin";
			public const char Dot = '.';
			public const string StateFilePath = "./StateInstApi/";
		}

		public static class ConnectionString
		{
			public const string Socion = "server=localhost;port=3306;database=socion;user=root;password=;SslMode=none";
			public const string Identity = "server=localhost;port=3306;database=socion_secure;user=root;password=;SslMode=none";
			public const string Hangfire = "server=localhost;port=3306;database=socion_hangfire;user=root;password=;SslMode=none;Allow User Variables=True";
		}

		public static class Jwt
		{
			public const string Issuer = "http://yourdomain.com";
			public const string Key = "Mc8UGtw1bLZ01pL9OhMq-DMsFGuoXfL8ZkV4wfMzf-SOCION-273naXajepYWPzEs13ty-td4mrb5PoZmfJtzKrYeK";
			public const int ExpireDays = 30;
		}

		public static class Role
		{
			public const string Admin = "admin";
			public const string CommonUser = "common-user";
		}

		public static class SocialNetwork
		{
			public static class Instagram
			{
				public const int Id = 0;
			}

			public static class Vk
			{
				public const int Id = 1;
			}
		}

		public static class MetricHelperLibrary
		{
			public static int MetricDefaultValue = 0;
			public static class JobName
			{
				public static string FlushMetricInst = "flush-metric-inst";
				public static string ZeroingSubscribeMetricInstagram = "zeroing-subscribe-metric-instagram";
				public static string ZeroingLikeMetricInstagram = "zeroing-like-metric-instagram";
			}
		}

		public static class EmailSender
		{
			public const string Smtp = "smtp.gmail.com";
			public const int SmtpPort = 587;
			public const bool UseSsl = false;
			public const string FromAddress = "socion.solutions@gmail.com";
			public const string FromName = "Администрация сайта";
			public const string FromPassword = "ilublumari";

			public static class EmailUpdating
			{
				public const string Message = @"Thank you for updating your email. Please confirm the email by clicking this link:
				<br><a href='{0}'>Confirm new email</a>";
			}

			public static class EmailConfirmation
			{
				public const string Subject = "Подвтерждение пароля";
			}
		}

		public static class Redis
		{
			public const string ConnectionString = "localhost:6379";
			public const char Delimiter = '|';

			public static class Prefix
			{
				public const string Email = "email_";
				public const string SubscribeByTagTask = "tags_";
				public const string AccountData = "account_data_";
				public const string SubscribtionPerDayInst = "subscribtion_per_day_inst_";
				public const string LikePerDayInst = "like_per_day_inst_";
				public const string UnsubscribtionPerDayInst = "unsubscribtion_per_day_inst_";
				public const string SubscribtionCompetitorPaginationInst = "subscribtion_competitor_pagination_inst_";
				public const string SubscribtionCompetitorInst = "subscribtion_competitor_inst_";
				public const string SubscribtionCompetitorsFollowersListInst = "subscribtion_competitors_followers_list_inst_";
				public const string SubscribtionCompetitorsFollowersNextPaginationIdInst = "subscribtion_competitors_followers_next_pagination_id_inst_";
				public const string SubscribtionByLocationsInst = "subscribtion_by_locations_inst_";
				public const string LikeByLocationsInst = "like_by_locations_inst_";
				public const string SubscribtionByLocationsGeoIdsInst = "subscribtion_by_locations_geo_ids_inst_";
				public const string LikeByLocationsGeoIdsInst = "like_by_locations_geo_ids_inst_";
				public const string SubscribtionByLocationsToSubInst = "subscribtion_by_locations_to_sub_inst_";
				public const string LikeByLocationsToLikeInst = "like_by_locations_to_like_inst_";
				public const string LastLikedFollowerInst = "last_liked_follower_inst_";
				public const string NextFollowersToLikeInst = "next_followers_to_like_inst_";
				public const string FollowersUsernamesToLikeInst = "followers_usernames_to_like_inst_";
				public const string LikeByTageInst = "like_by_tag_inst_";
				public const string Log = "log_";
			}
		}

		public static class RabbitMq
		{
			public const string ConnectionString = "localhost";

			public static class ExchangeName
			{
				public const string Email = "email";
				public const string SubscribeByTagTaskInst = "subscribe-by-tag-task-isnt";
				public const string SubscribeByCompetitorTaskInst = "subscribe-by-competitor-task-inst";
				public const string SubscribeByLocationsTaskInst = "subscribe-by-competitor-task-inst";
				public const string LikeByLocationsTaskInst = "like-by-competitor-task-inst";
				public const string UnsubscribeTaskInst = "unsubscribe-task-inst";
				public const string LikeFollowersTaskInst = "like-followers-task-inst";
				public const string LikeByTagTaskInst = "like-by-tag-task-inst";
				public const string Log = "log";
			}

			public static class QueueName
			{
				public const string Log = "email";
			}

			public const char Delimiter = '|';
		}

		public static class Controller
		{
			public static class Auth
			{
				public const string Route = "auth";

				public static class SignIn
				{
					public const string Route = "sign-in";
				}

				public static class SignUp
				{
					public const string Route = "sign-up";
				}

				public static class Confirmation
				{
					public const string Route = "confirmation";
				}

				public static class SendConfirmation
				{
					public const string Route = "send-confirmation";
				}
			}

			public static class ProxiesManagerController
			{
				public const string Produces = "application/json";
				public const string Route = "proxy-manager";

				public static class SignIn
				{
					public const string Route = "sign-in";
				}
			}

			public static class InstagramPromotion
			{
				public const string Produces = "application/json";
				public const string Route = "instagram-promotion";

				public static class Post
				{
					public const string Route = "by-tags";
				}
				public static class SubscribeByCompetitor
				{
					public const string Route = "by-competitors";
				}
				public static class SubscribeByLocataions
				{
					public const string Route = "by-locations";
				}
				public static class LikeByLocataions
				{
					public const string Route = "like-by-locations";
				}
				public static class LikeFollowers
				{
					public const string Route = "like-followers";
				}
				public static class LikeByTag
				{
					public const string Route = "like-by-tag";
				}

				public static class Put
				{
					public const string Route = "{id}";
				}

				public static class Delete
				{
					public const string Route = "{id}";
				}
			}

			public static class SocialNetworkAccount
			{
				public const string Produces = "application/json";
				public const string Route = "accounts";

				public static class DeleteAsync
				{
					public const string Route = "{id}";
				}
			}

			public static class SocialPromotion
			{
				public const string Produces = "application/json";
				public const string Route = "promotion";
				public static class GetAllUserPromotionTasks
				{
					public const string Route = "get-tasks";
				}

				public static class GetUserPromotionTasksBySocialNetworkId
				{
					public const string Route = "get-task-by-social-network-id/{socialNetworkId:int}";
				}
				public static class GetPromotionTask
				{
					public const string Route = "get-task/{promotionTaskId:int}";
				}

				public static class ChangeRecurringPeriodOfTask
				{
					public const string Route = "change-recurring-period-task";
				}

				public static class Put
				{
					public const string Route = "{id}";
				}

				public static class Delete
				{
					public const string Route = "{taskId:int}";
				}
			}

			public static class UserServices
			{
				public const string Produces = "application/json";
				public const string Route = "services";

				public static class GetUsersServices
				{
					public const string Route = "get-all";
				}

				public static class GetUserServices
				{
					public const string Route = "my-services";
				}
				public static class GetUserServicesById
				{
					public const string Route = "{userId:int}";
				}

				public static class AddUserService
				{
					public const string Route = "{serviceId:int}";
				}

				public static class DeleteUserService
				{
					public const string Route = "{serviceId:int}";
				}
			}

			public static class UserSettings
			{
				public const string Produces = "application/json";
				public const string Route = "user-settings";
			}

			public static class Values
			{
				public const string Route = "values";

				public static class Get
				{
					public const string Route = "{id}";
				}

				public static class Put
				{
					public const string Route = "{id}";
				}

				public static class Delete
				{
					public const string Route = "{id}";
				}
			}
			public static class MetricHelperController
			{
				public const string Route = "metric-helper";

				public const string Produces = "application/json";
				public static class RunLikeMetricFlush
				{
					public const string Route = "run-flush-inst";
				}

				public static class AddJobMetricFlush
				{
					public const string Route = "add-job-flush-inst";
				}

				public static class GetLikeMetricPerDay
				{
					public const string Route = "get-like-metric/{id:int}";
				}
				public static class GetSubMetricPerDay
				{
					public const string Route = "get-sub-metric/{id:int}";
				}
				public static class GetUnsubMetricPerDay
				{
					public const string Route = "get-unsub-metric/{id:int}";
				}
			}
			
		}

		public static class Dto
		{
			public static class Shared
			{
				public static class Email
				{
					public const string Name = "email";
					public const int MinLength = 6;
					public const int MaxLength = 256;
				}

				public static class Password
				{
					public const string Name = "password";
					public const int MinLength = 6;
					public const int MaxLength = 32;
				}

				public static class Username
				{
					public const string Name = "username";
					public const int MinLength = 2;
					public const int MaxLength = 32;
				}
			}

			public static class Proxy
			{
				public static class ProxyId
				{
					public const string Name = "proxyId";
				}
				public static class Value
				{
					public const string Name = "value";
				}
				public static class IsValid
				{
					public const string Name = "isValid";
				}
				public static class Login
				{
					public const string Name = "login";
				}
				public static class Password
				{
					public const string Name = "password";
				}
				public static class Type
				{
					public const string Name = "type";
				}
				public static class ExpirationDate
				{
					public const string Name = "expirationDate";
				}
			}
			public static class UserProxy
			{
				public static class ProxyId
				{
					public const string Name = "proxyId";
				}

				public static class UserId
				{
					public const string Name = "userId";
				}
			}

			public static class PromotionTaskSettingsDto
			{
				public static class PromotionTaskId
				{
					public const string Name = "promotionTaskId";
				}

				public static class Cron
				{
					public const string Name = "cron";
				}
			}
			public static class ProxyManager
			{
				public static class Proxy
				{
					public const string Name = "proxy";
				}
				public static class SocialNetworkAccount
				{
					public const string Name = "socialNetworkAccount";
				}
			}
			public static class SocialNetworkAccountProxy
			{
				public static class ProxyId
				{
					public const string Name = "proxyId";
				}
				public static class SocialNetworkAccountId
				{
					public const string Name = "socialNetworkAccountId";
				}
			}

			public static class PromotionTask
			{
				public static class PromotionTaskId
				{
					public const string Name = "promotionTaskId";
					public const int Default = 0;
				}
				public static class SocialNetworkAccountId
				{
					public const string Name = "socialNetworkAccountId";
				}
				public static class LikeAmmount
				{
					public const string Name = "likeAmmount";
					public const int Default = 0;

				}
				public static class PromotionTaskType
				{
					public const string Name = "promotionTaskType";
				}
				public static class SubscribeByTagsInstTaskDto
				{
					public const string Name = "subscribeByTagsInstTaskDto";
				}
			}

			public static class SocialNetworkAccount
			{
				public static class SocialNetworkAccountId
				{
					public const string Name = "socialNetworkAccountId";
					public const int Default = 0;
				}

				public static class SocialNetworkId
				{
					public const string Name = "socialNetworkId";
				}

				public static class AccessToken
				{
					public const string Name = "accessToken";
				}

				public static class TokenLifetime
				{
					public const string Name = "tokenLifetime";
				}

				public static class Login
				{
					public const string Name = "login";
				}

				public static class Proxy
				{
					public const string Name = "proxy";
				}
			}
			public static class SubscribeByCompetitorInstTaskDto
			{
				public static class Competitors
				{
					public const string Name = "competitors";
				}

				public static class DaysToDelete
				{
					public const string Name = "daysToDelete";
				}
			}
			public static class SubscribeByLocationsInstTaskDto
			{
				public static class Locations
				{
					public const string Name = "locations";
				}

				public static class DaysToDelete
				{
					public const string Name = "daysToDelete";
				}
			}

			public static class SubscribeByTagsInstTask
			{
				public static class HashTags
				{
					public const string Name = "hashTags";
				}

				public static class DaysToDelete
				{
					public const string Name = "daysToDelete";
				}
			}
			public static class LikeByTagsInstTaskDto
			{
				public static class HashTags
				{
					public const string Name = "hashTags";
				}
			}

			public static class UserSettings
			{
				public static class NewPassword
				{
					public const string Name = "newPassword";
				}

				public static class CurrentPassword
				{
					public const string Name = "currentPassword";
				}

				public static class EmailConfirmed
				{
					public const string Name = "emailConfirmed";
				}
			}
		}
	}
}
