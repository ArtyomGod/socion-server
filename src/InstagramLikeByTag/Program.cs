﻿using System;
using System.Diagnostics;
using System.Linq;
using ConstantLibrary;
using EntityFramework;
using Hangfire;
using Hangfire.MySql.Core;
using Model;
using RabbitMqLibrary;
using RedisLibrary;
using Socion.DataPreparator;
using Socion.Helper.SocialNetworkApiHelper.InstagramApiHelper;

namespace InstagramLikeByTag
{
    class Program
    {
        static void Main(string[] args)
        {
			GlobalConfiguration.Configuration.UseStorage(new MySqlStorage(Constant.ConnectionString.Hangfire));

			var rabbitMq = new RabbitMq();
			rabbitMq.QueueDeclare();
			rabbitMq.ExchangeDeclare(Constant.RabbitMq.ExchangeName.LikeByTagTaskInst, ExchangeType.Fanout);
			rabbitMq.BindQueueToExchange(Constant.RabbitMq.ExchangeName.LikeByTagTaskInst);
			rabbitMq.ConsumeQueue(message =>
			{
				var watch = Stopwatch.StartNew();

				var splitedMessage = message.Split(Constant.RabbitMq.Delimiter);
				if (splitedMessage.Length != 2)
				{
					throw new ArgumentException();
				}
				int accountId = Convert.ToInt32(splitedMessage.First());
				int likeAmmount = Convert.ToInt32(splitedMessage[1]);

				string redisKey = $"{Constant.Redis.Prefix.LikeByTageInst}{accountId}";
				string tags = Redis.Instance.Database.StringGet(redisKey);
				string redisMetricKeyLike = $"{Constant.Redis.Prefix.LikePerDayInst}{accountId}";
				var madeLikeAmmount = Redis.Instance.Database.StringGet(redisMetricKeyLike);
				var maxAmmoutnToLike = GetMaximumAllowableValue(Convert.ToInt32(madeLikeAmmount), 3, Constant.SocialNetworkConstraints.MaxAmmountToLikePerDayInst);
				if (maxAmmoutnToLike <= 0)
				{
					Console.WriteLine($"Account with id: {accountId} reachd Limit Of Liking for today");
					return;
				}
				string tag = RemoveFirstTag(ref tags);
				Redis.Instance.Database.StringSet(redisKey, tags);

				SocialNetworkAccount socialNetworkAccount;
				Proxy proxy = new Proxy();
				using (var context = new SocionContext())
				{
					socialNetworkAccount = context.SocialNetworkAccounts.FirstOrDefault(account => account.SocialNetworkAccountId == accountId);
					var proxyId = context.SocialNetworkAccountProxies.Where(a => a.SocialNetworkAccountId == socialNetworkAccount.SocialNetworkAccountId).Select(a => a.ProxyId).FirstOrDefault();

					proxy = context.Proxies.FirstOrDefault(a => a.ProxyId == proxyId);
				}
				IInstagramApiHelper apiHelper = new InstagramApiHelper(socialNetworkAccount, proxy);
				PaginationUserInfo paginationUserInfo;
				string nextId = string.Empty;
				do
				{
					paginationUserInfo = apiHelper.FindUserByTag(tag, nextId);
					nextId = paginationUserInfo.StartFromId;
				} while (!apiHelper.LikeAmmountOfMedia(paginationUserInfo.User.UserName, maxAmmoutnToLike));
				
				BackgroundJob.Enqueue(() => Redis.Increment(redisMetricKeyLike, maxAmmoutnToLike));

				watch.Stop();
				long elapsedMs = watch.ElapsedMilliseconds;
				Console.WriteLine($"Liked for {paginationUserInfo.User.UserName} from {socialNetworkAccount.Login}; by tag: {tag} ;UnsubscribeAfterDays:{likeAmmount} ;Time: {elapsedMs}");
			});

			Console.WriteLine("InstagramLikeByTagListener has started");
			Console.WriteLine("Press [enter] to exit.");
			Console.ReadKey();
		}

		private static int GetMaximumAllowableValue(int madeLikeAmmount, int ammountToMake, int constraint)
		{
			var newValue = madeLikeAmmount + ammountToMake;
			if (newValue > constraint)
			{
				return constraint - newValue;
			}
			return ammountToMake;
		}


		private static string RemoveFirstTag(ref string tags)
		{
			string firstTag = string.Empty;
			int tagLastIndex = 0;
			foreach (char ch in tags)
			{
				++tagLastIndex;
				if (ch == ';')
				{
					break;
				}
				firstTag += ch;
			}
			tags = tags.Remove(0, tagLastIndex);

			return firstTag;
		}
	}
}
