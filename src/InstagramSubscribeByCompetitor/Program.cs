﻿using System;
using System.Data.SqlTypes;
using System.Linq;
using EntityFramework;
using Hangfire;
using Hangfire.MySql.Core;
using Model;
using RabbitMqLibrary;
using RedisLibrary;
using Socion.DataPreparator;
using Socion.Helper.SocialNetworkApiHelper.InstagramApiHelper;
using Constant = ConstantLibrary.Constant;

namespace InstagramSubscribeByCompetitor
{
	class Program
	{
		static void Main(string[] args)
		{
			GlobalConfiguration.Configuration.UseStorage(new MySqlStorage(Constant.ConnectionString.Hangfire));

			var rabbitMq = new RabbitMq();
			rabbitMq.QueueDeclare();
			rabbitMq.ExchangeDeclare(Constant.RabbitMq.ExchangeName.SubscribeByCompetitorTaskInst, ExchangeType.Fanout);
			rabbitMq.BindQueueToExchange(Constant.RabbitMq.ExchangeName.SubscribeByCompetitorTaskInst);
			rabbitMq.ConsumeQueue(message =>
			{
				var splitedMessage = DelimRabbitMqString(message, 2);
				int accountId = Convert.ToInt32(splitedMessage.First());
				int daysToUnsubscribe = Convert.ToInt32(splitedMessage[1]);

				string redisKey = $"{Constant.Redis.Prefix.SubscribtionCompetitorInst}{accountId}";
				string competitors = Redis.Instance.Database.StringGet(redisKey);
				string competitor = RemoveFirstFollower(ref competitors);
				Redis.Instance.Database.StringSet(redisKey, competitors);

				string subscribtionCompetitorsFollowersListRedisKey = $"{Constant.Redis.Prefix.SubscribtionCompetitorsFollowersListInst}{accountId}_{competitor}";
				string competitorFollowersList = Redis.Instance.Database.StringGet(subscribtionCompetitorsFollowersListRedisKey);

				string subscribtionCompetitorsFollowersNextIdRedisKey = $"{Constant.Redis.Prefix.SubscribtionCompetitorsFollowersNextPaginationIdInst}{accountId}_{competitor}";
				string nextPaginationId = Redis.Instance.Database.StringGet(subscribtionCompetitorsFollowersListRedisKey);

				string redisMetricKeySusbcribtion = $"{Constant.Redis.Prefix.SubscribtionPerDayInst}{accountId}";
				var madeSubsAmmount = Redis.Instance.Database.StringGet(redisMetricKeySusbcribtion);
				var maxAmmoutnToSub = GetMaximumAllowableValue(Convert.ToInt32(madeSubsAmmount), 1, Constant.SocialNetworkConstraints.MaxAmmountToSubscribePerDayInst);
				if (maxAmmoutnToSub <= 0)
				{
					Console.WriteLine($"Account with id: {accountId} reachd Limit Of Subscribing for today");
					return;
				}
				string redisMetricKeyLike = $"{Constant.Redis.Prefix.LikePerDayInst}{accountId}";
				var madeLikeAmmount = Redis.Instance.Database.StringGet(redisMetricKeyLike);

				SocialNetworkAccount socialNetworkAccount;
				Proxy proxy;
				using (var context = new SocionContext())
				{
					socialNetworkAccount = context.SocialNetworkAccounts.FirstOrDefault(account => account.SocialNetworkAccountId == accountId);
					var proxyId = context.SocialNetworkAccountProxies.Where(a => a.SocialNetworkAccountId == socialNetworkAccount.SocialNetworkAccountId)
						.Select(a => a.ProxyId).FirstOrDefault();

					proxy = context.Proxies.FirstOrDefault(a => a.ProxyId == proxyId);
				}
				if (socialNetworkAccount == null)
				{
					Console.WriteLine($"Account with id {accountId} wasn`t found");
					throw new SqlNullValueException();
				}

				IInstagramApiHelper apiHelper = new InstagramApiHelper(socialNetworkAccount, proxy);
				if(!apiHelper.IsUserAuthenticated())
				{
					Console.WriteLine($"Cannot login instagram account {socialNetworkAccount.SocialNetworkAccountId}");
					return;
				}
				if (string.IsNullOrEmpty(competitorFollowersList))
				{
					var followersList = apiHelper.GetUserFollowersList(competitor, nextPaginationId);
					competitorFollowersList = followersList[0];

					Redis.Instance.Database.StringSet(subscribtionCompetitorsFollowersNextIdRedisKey, followersList[1]);
				}

				var competitorsFollower = RemoveFirstFollower(ref competitorFollowersList);
				while ((apiHelper.IsFollowedByUser(competitorsFollower) || string.IsNullOrEmpty(competitorsFollower) 
				&& !string.IsNullOrEmpty(competitorFollowersList)))
				{
					competitorsFollower = RemoveFirstFollower(ref competitorFollowersList);
				}
				Redis.Instance.Database.StringSet(subscribtionCompetitorsFollowersListRedisKey, competitorFollowersList);

				if (string.IsNullOrEmpty(competitorsFollower))
				{
					Console.WriteLine($"Didn`t get competitor`s follower to subscribe");
					return;
				}
				var userToSubscribe = apiHelper.GetUser(competitorsFollower);
				if(!apiHelper.Subscribe(userToSubscribe.Pk))
				{
					Console.WriteLine(
					$"Didn`t subscribe for {userToSubscribe.UserName} from {socialNetworkAccount.Login}; UnsubscribeAfterDays:{daysToUnsubscribe}");			
					return;
				}
				
				InstagramDataPreparator instagramDataPreparator = new InstagramDataPreparator();
				BackgroundJob.Schedule(
					() => instagramDataPreparator.UnsubscribeDataPreparation(userToSubscribe.Pk, socialNetworkAccount.SocialNetworkAccountId),
					TimeSpan.FromDays(daysToUnsubscribe));

				Console.WriteLine(
					$"Subscribe for {userToSubscribe.UserName} from {socialNetworkAccount.Login}; UnsubscribeAfterDays:{daysToUnsubscribe}");
				BackgroundJob.Enqueue(() => Redis.Increment(redisMetricKeySusbcribtion));
				var maxAmmoutnToLike = GetMaximumAllowableValue(Convert.ToInt32(madeLikeAmmount), 3, Constant.SocialNetworkConstraints.MaxAmmountToLikePerDayInst);
				if (maxAmmoutnToLike <= 0)
				{
					Console.WriteLine($"Account with id: {accountId} reachd Limit Of Liking for today");
					return;
				}

				apiHelper.LikeAmmountOfMedia(userToSubscribe.UserName, 3);
				BackgroundJob.Enqueue(() => Redis.Increment(redisMetricKeyLike, 3));
				Console.WriteLine($"Liked after subscribe 3 times");
			});

			Console.WriteLine("InstagramSubscribeByCompetitorListener has started");
			Console.WriteLine("Press [enter] to exit.");
			Console.ReadKey();
		}

		private static int GetMaximumAllowableValue(int madeLikeAmmount, int ammountToMake, int constraint)
		{
			var newValue = madeLikeAmmount + ammountToMake;
			if (newValue > constraint)
			{
				return constraint - newValue;
			}
			return ammountToMake;
		}

		private static string RemoveFirstFollower(ref string tags)
		{
			string firstTag = string.Empty;
			int tagLastIndex = 0;
			foreach (char ch in tags)
			{
				++tagLastIndex;
				if (ch == ';')
				{
					break;
				}
				firstTag += ch;
			}
			tags = tags.Remove(0, tagLastIndex);

			return firstTag;
		}

		private static string[] DelimRabbitMqString(string input, int ammountOfArguments)
		{
			var splitedMessage = input.Split(Constant.RabbitMq.Delimiter);
			if (splitedMessage.Length != ammountOfArguments)
			{
				throw new ArgumentException();
			}

			return splitedMessage;
		}
	}
}
