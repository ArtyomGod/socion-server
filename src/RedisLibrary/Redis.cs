﻿using System;
using ConstantLibrary;
using StackExchange.Redis;

namespace RedisLibrary
{
	public class Redis
	{
		public static readonly Redis Instance = new Redis();

		public IDatabase Database { get; }

		private Redis()
		{
			Database = ConnectionMultiplexer.Connect(Constant.Redis.ConnectionString).GetDatabase();
		}
		public static void Increment(string key)
		{
			var database = ConnectionMultiplexer.Connect(Constant.Redis.ConnectionString).GetDatabase();
			int value = Convert.ToInt32(database.StringGet(key));
			value++;
			database.StringSet(key, value);
		}
		public static void Increment(string key, int count)
		{
			for (int i = 0; i < count; ++i)
			{
				Increment(key);
			}
		}
	}
}

