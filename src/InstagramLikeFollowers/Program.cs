﻿using System;
using System.Diagnostics;
using System.Linq;
using ConstantLibrary;
using EntityFramework;
using Hangfire;
using Hangfire.MySql.Core;
using Model;
using RabbitMqLibrary;
using RedisLibrary;
using Socion.DataPreparator;
using Socion.Helper.SocialNetworkApiHelper.InstagramApiHelper;


namespace InstagramLikeFollowers
{
	class Program
	{
		static void Main(string[] args)
		{
			GlobalConfiguration.Configuration.UseStorage(new MySqlStorage(Constant.ConnectionString.Hangfire));

			var rabbitMq = new RabbitMq();
			rabbitMq.QueueDeclare();
			rabbitMq.ExchangeDeclare(Constant.RabbitMq.ExchangeName.LikeFollowersTaskInst, ExchangeType.Fanout);
			rabbitMq.BindQueueToExchange(Constant.RabbitMq.ExchangeName.LikeFollowersTaskInst);
			rabbitMq.ConsumeQueue(message =>
			{
				var watch = Stopwatch.StartNew();

				var splitedMessage = message.Split(Constant.RabbitMq.Delimiter);
				if (splitedMessage.Length != 2)
				{
					throw new ArgumentException();
				}
				int accountId = Convert.ToInt32(splitedMessage.First());
				int likeAmmount = Convert.ToInt32(splitedMessage[1]);
				
				string followersUsernamesToLikeInstRedisKey = $"{Constant.Redis.Prefix.FollowersUsernamesToLikeInst}{accountId}";
				string nextFollowersToLikeRedisKey = $"{Constant.Redis.Prefix.NextFollowersToLikeInst}{accountId}";
				string followersUsernamesToLike = Redis.Instance.Database.StringGet(followersUsernamesToLikeInstRedisKey);
				string nextFollowersToLike = Redis.Instance.Database.StringGet(nextFollowersToLikeRedisKey);
				string redisMetricKeyLike = $"{Constant.Redis.Prefix.LikePerDayInst}{accountId}";

				var madeLikeAmmount = Redis.Instance.Database.StringGet(redisMetricKeyLike);
				var maxAmmoutnToLike = GetMaximumAllowableValue(Convert.ToInt32(madeLikeAmmount), likeAmmount);
				if (maxAmmoutnToLike <= 0)
				{
					Console.WriteLine($"Account with id: {accountId} reachd Limit Of Liking for today");
					return;
				}
				SocialNetworkAccount socialNetworkAccount;
				Proxy proxy;
				using (var context = new SocionContext())
				{
					socialNetworkAccount = context.SocialNetworkAccounts.FirstOrDefault(account => account.SocialNetworkAccountId == accountId);
					var proxyId = context.SocialNetworkAccountProxies.Where(a => a.SocialNetworkAccountId == socialNetworkAccount.SocialNetworkAccountId)
						.Select(a => a.ProxyId).FirstOrDefault();

					proxy = context.Proxies.FirstOrDefault(a => a.ProxyId == proxyId);
				}
				IInstagramApiHelper apiHelper = new InstagramApiHelper(socialNetworkAccount, proxy);

				if (string.IsNullOrEmpty(followersUsernamesToLike))
				{
					var followersList = apiHelper.GetCurrentUserFollowersList(nextFollowersToLike);
					followersUsernamesToLike = followersList[0];

					Redis.Instance.Database.StringSet(nextFollowersToLikeRedisKey, followersList[1]);
				}

				var follower = RemoveFirstFollower(ref followersUsernamesToLike);
				while (!apiHelper.IsFollowedByUser(follower))
				{
					follower = RemoveFirstFollower(ref followersUsernamesToLike);
				}
				Redis.Instance.Database.StringSet(followersUsernamesToLikeInstRedisKey, followersUsernamesToLike);

				if (string.IsNullOrEmpty(follower))
				{
					return;
				}

				apiHelper.LikeAmmountOfMedia(follower, maxAmmoutnToLike);
				BackgroundJob.Enqueue(() => Redis.Increment(redisMetricKeyLike, likeAmmount));
				
				watch.Stop();
				long elapsedMs = watch.ElapsedMilliseconds;
				Console.WriteLine(
					$"Liked for {follower} from {socialNetworkAccount.Login}; Liked {likeAmmount}; Time: {elapsedMs}");
			});

			Console.WriteLine("InstagramLikeFollowersListener has started");
			Console.WriteLine("Press [enter] to exit.");
			Console.ReadKey();
		}

		private static int GetMaximumAllowableValue(int madeLikeAmmount, int ammountToMake)
		{
			var newValue = madeLikeAmmount + ammountToMake;
			if (newValue > Constant.SocialNetworkConstraints.MaxAmmountToLikePerDayInst)
			{
				return Constant.SocialNetworkConstraints.MaxAmmountToLikePerDayInst - newValue;
			}
			return ammountToMake;
		}

		private static string RemoveFirstFollower(ref string tags)
		{
			string firstTag = string.Empty;
			int tagLastIndex = 0;
			foreach (char ch in tags)
			{
				++tagLastIndex;
				if (ch == ';')
				{
					break;
				}
				firstTag += ch;
			}
			tags = tags.Remove(0, tagLastIndex);

			return firstTag;
		}

		private string GetActiveFollower(ref string followers)
		{
			string follower = RemoveFirstFollower(ref followers);

			return follower;
		}
	}
}
