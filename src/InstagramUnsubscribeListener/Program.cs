﻿using System;
using System.Data.SqlTypes;
using System.Linq;
using System.Text;
using ConstantLibrary;
using RabbitMqLibrary;
using Socion.Helper.SocialNetworkApiHelper.InstagramApiHelper;
using EntityFramework;
using RedisLibrary;

namespace InstagramUnsubscribeListener
{
	static class Program
	{
		static void Main(string[] args)
		{
			var rabbitMq = new RabbitMq();
			rabbitMq.QueueDeclare();
			rabbitMq.ExchangeDeclare(Constant.RabbitMq.ExchangeName.UnsubscribeTaskInst, ExchangeType.Fanout);
			rabbitMq.BindQueueToExchange(Constant.RabbitMq.ExchangeName.UnsubscribeTaskInst);
			rabbitMq.ConsumeQueue(message =>
			{
				var splitedMessage = message.Split(Constant.RabbitMq.Delimiter);
				if (splitedMessage.Length != 2)
				{
					throw new ArgumentException();
				}

				long unsubscribeLogin = Convert.ToInt64(splitedMessage.First());
				int accountId = Convert.ToInt32(splitedMessage[1]);

				string redisMetricKeyUnsusbcribtion = $"{Constant.Redis.Prefix.UnsubscribtionPerDayInst}{accountId}";
				var madeSubsAmmount = Redis.Instance.Database.StringGet(redisMetricKeyUnsusbcribtion);
				var maxAmmoutnToSub = GetMaximumAllowableValue(Convert.ToInt32(madeSubsAmmount), 1, Constant.SocialNetworkConstraints.MaxAmmountToUnsubscribePerDayInst);
				if (maxAmmoutnToSub <= 0)
				{
					Console.WriteLine($"Account with id: {accountId} reachd Limit Of UnSubscribing for today");
					return;
				}
				SocialNetworkAccount socialNetworkAccount;
				Proxy proxy;
				using (var context = new SocionContext())
				{
					socialNetworkAccount = context.SocialNetworkAccounts.FirstOrDefault(account => account.SocialNetworkAccountId == accountId);
					var proxyId = context.SocialNetworkAccountProxies.Where(a => a.SocialNetworkAccountId == socialNetworkAccount.SocialNetworkAccountId).Select(a => a.ProxyId).FirstOrDefault();

					proxy = context.Proxies.FirstOrDefault(a => a.ProxyId == proxyId);
				}
				if (socialNetworkAccount == null)
				{
					throw new SqlNullValueException();
				}

				IInstagramApiHelper apiHelper = new InstagramApiHelper(socialNetworkAccount, proxy);
				apiHelper.Unsubscribe(unsubscribeLogin);
				Redis.Increment(redisMetricKeyUnsusbcribtion);

				Console.WriteLine($"Unsubscribed id: {unsubscribeLogin}");
			});
			
			Console.WriteLine("InstagramUnsubscribeListener has started");
			Console.WriteLine("Press [enter] to exit.");
			Console.ReadKey();
		}
		private static int GetMaximumAllowableValue(int madeLikeAmmount, int ammountToMake, int constraint)
		{
			var newValue = madeLikeAmmount + ammountToMake;
			if (newValue > constraint)
			{
				return constraint - newValue;
			}
			return ammountToMake;
		}

	}
}
