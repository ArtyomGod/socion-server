﻿using System;
using System.IO;
using ConstantLibrary;
using RabbitMqLibrary;
using RedisLibrary;

namespace LogListener
{
	class Program
	{
		private static readonly string LoggerIdentifier = GetId();
		private static readonly StreamWriter LogFile = new StreamWriter($"Log.{LoggerIdentifier}.log");
		
		static void Main(string[] args)
		{
			var rabbitMq = new RabbitMq();
			rabbitMq.QueueDeclare(Constant.RabbitMq.QueueName.Log);
			rabbitMq.ExchangeDeclare(Constant.RabbitMq.ExchangeName.Log, ExchangeType.Fanout);
			rabbitMq.BindQueueToExchange(Constant.RabbitMq.ExchangeName.Log);
			rabbitMq.ConsumeQueue(message =>
			{
				string redisKey = $"{Constant.Redis.Prefix.Log}{message}";
				string logString = Redis.Instance.Database.StringGet(redisKey);
				string[] fields = logString.Split(Constant.Redis.Delimiter);
				Redis.Instance.Database.KeyDelete(redisKey);

				WriteLogToFile(fields);
			});

			Console.WriteLine($"LogListener {LoggerIdentifier} has started");
			Console.WriteLine("Press [enter] to exit.");
			Console.ReadKey();
		}

		private static void WriteLogToFile(string[] fields)
		{
			foreach (string field in fields)
			{
				LogFile.Write(field);
				LogFile.Write("|");
			}
			LogFile.WriteLine();
			LogFile.Flush();
		}

		private static string GetId()
		{
			uint identifier = 0;
			while (File.Exists($"Log.{identifier}.log"))
			{
				++identifier;
			}

			return identifier.ToString();
		}
	}
}
