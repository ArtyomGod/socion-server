﻿using System;
using Hangfire;
using Hangfire.MySql.Core;
using MetricHelperLibrary;

namespace HangfireServer
{
    class Program
    {
        static void Main(string[] args)
		{
			GlobalConfiguration.Configuration.UseStorage(new MySqlStorage(ConstantLibrary.Constant.ConnectionString.Hangfire));

			using (var server = new BackgroundJobServer())
	        {
		        Console.WriteLine("Hangfire Server started. Press any key to exit...");
		        Console.ReadKey();
		        Console.WriteLine("OUT!!!");
			}
		}
    }
}
