﻿using System;
using System.Linq;
using ConstantLibrary;
using EntityFramework;
using RedisLibrary;

namespace MetricHelperLibrary
{
    public class MetricHelper
    {
		public static void FlushInstMetrics()
	    {
		    using (var context = new SocionContext())
			{
				Redis redisHelper = Redis.Instance;
				var accountIds = context.SocialNetworkAccounts.Where(acc => acc.SocialNetworkId == (int)SocialNetwork.Instagram).Select(ac => ac.SocialNetworkAccountId).ToList();
			    foreach (var accountId in accountIds)
			    {
				    redisHelper.Database.StringSet($"{Constant.Redis.Prefix.SubscribtionPerDayInst}{accountId}", Constant.MetricHelperLibrary.MetricDefaultValue);
				    redisHelper.Database.StringSet($"{Constant.Redis.Prefix.LikePerDayInst}{accountId}", Constant.MetricHelperLibrary.MetricDefaultValue);
				    redisHelper.Database.StringSet($"{Constant.Redis.Prefix.UnsubscribtionPerDayInst}{accountId}", Constant.MetricHelperLibrary.MetricDefaultValue);
			    }
		    }
	    }

		public static int GetLikeMetricInstagram(int accountId)
		{				
			Redis redisHelper = Redis.Instance;

			var likeMetric = redisHelper.Database.StringGet($"{Constant.Redis.Prefix.LikePerDayInst}{accountId}");
			return string.IsNullOrEmpty(likeMetric) ? 0 : Convert.ToInt32(likeMetric);
		}

		public static int GetSubMetricInstagram(int accountId)
		{
			Redis redisHelper = Redis.Instance;

			var subMetric = redisHelper.Database.StringGet($"{Constant.Redis.Prefix.SubscribtionPerDayInst}{accountId}");
			return string.IsNullOrEmpty(subMetric) ? 0 : Convert.ToInt32(subMetric);
		}		

		public static int GetUnsubMetricInstagram(int accountId)
		{
			Redis redisHelper = Redis.Instance;

			var unsubMetric = redisHelper.Database.StringGet($"{Constant.Redis.Prefix.UnsubscribtionPerDayInst}{accountId}");
			return string.IsNullOrEmpty(unsubMetric) ? 0 : Convert.ToInt32(unsubMetric);
		}
    }
}
