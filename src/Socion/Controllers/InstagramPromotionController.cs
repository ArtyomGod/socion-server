﻿using System;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using ConstantLibrary;
using EntityFramework;
using Hangfire;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Socion.DataPreparator;
using Socion.Dto;

namespace Socion.Controllers
{
	[Produces(Constant.Controller.InstagramPromotion.Produces)]
	[Route(Constant.Controller.InstagramPromotion.Route)]
	[Authorize]
	public class InstagramPromotionController : Controller
	{
		private readonly SocionContext _db;
		private readonly IMapper _mapper;
		private readonly UserManager<ApplicationUser> _userManager;

		private enum ResultCode
		{
			InternalError
		}

		public InstagramPromotionController(
			SocionContext db,
			IMapper mapper,
			UserManager<ApplicationUser> userManager)
		{
			_db = db;
			_mapper = mapper;
			_userManager = userManager;
		}

		[HttpPost]
		[Route(Constant.Controller.InstagramPromotion.Post.Route)]
		public async Task<IActionResult> Post([FromBody] SubscribeByTagsInstTaskDto model)
		{
			if (!ModelState.IsValid)
			{
				return BadRequest(ModelState);
			}

			PromotionTask promotionTask = _mapper.Map<SubscribeByTagsInstTaskDto, PromotionTask>(model);

			if (IsPromotionTaskExists(promotionTask))
			{
				ApplicationUser user = await _userManager.GetUserAsync(User);
				if (!_db.SocialNetworkAccounts.Any(account => account.UserId == user.Id))
				{
					return BadRequest();
				}

				_db.PromotionTasks.Update(promotionTask);
			}
			else
			{
				_db.PromotionTasks.Add(promotionTask);
				try
				{
					_db.SaveChanges();
				}
				catch (DbUpdateException)
				{
					return BadRequest(ResultCode.InternalError);
				}

				if (!SaveToAccountPromotionTasks(model.SocialNetworkAccountId, promotionTask.PromotionTaskId))
				{
					return BadRequest(ResultCode.InternalError);
				}
			}

			try
			{
				_db.SaveChanges();
			}
			catch (DbUpdateException)
			{
				return BadRequest(ResultCode.InternalError);
			}


			var dataPreparator = new InstagramDataPreparator();

			RecurringJob.AddOrUpdate($"{promotionTask.PromotionTaskId}", () =>
				dataPreparator.SubscribeByTagDataPreparation(promotionTask.PromotionTaskId), Cron.MinuteInterval(1));

			return Ok();
		}

		[HttpPost]
		[Route(Constant.Controller.InstagramPromotion.SubscribeByCompetitor.Route)]
		public async Task<IActionResult> SubscribeByCompetitor([FromBody] SubscribeByCompetitorInstTaskDto model)
		{
			if (!ModelState.IsValid)
			{
				return BadRequest(ModelState);
			}

			PromotionTask promotionTask = _mapper.Map<SubscribeByCompetitorInstTaskDto, PromotionTask>(model);

			if (IsPromotionTaskExists(promotionTask))
			{
				ApplicationUser user = await _userManager.GetUserAsync(User);
				if (!_db.SocialNetworkAccounts.Any(account => account.UserId == user.Id))
				{
					return BadRequest();
				}

				_db.PromotionTasks.Update(promotionTask);
			}
			else
			{
				_db.PromotionTasks.Add(promotionTask);
				try
				{
					_db.SaveChanges();
				}
				catch (DbUpdateException)
				{
					return BadRequest(ResultCode.InternalError);
				}

				if (!SaveToAccountPromotionTasks(model.SocialNetworkAccountId, promotionTask.PromotionTaskId))
				{
					return BadRequest(ResultCode.InternalError);
				}
			}

			try
			{
				_db.SaveChanges();
			}
			catch (DbUpdateException)
			{
				return BadRequest(ResultCode.InternalError);
			}


			var dataPreparator = new InstagramDataPreparator();

			RecurringJob.AddOrUpdate($"{promotionTask.PromotionTaskId}", () =>
				dataPreparator.SubscribeByCompetitorDataPreparation(promotionTask.PromotionTaskId), Cron.MinuteInterval(1));

			return Ok();
		}

		[HttpPost]
		[Route(Constant.Controller.InstagramPromotion.SubscribeByLocataions.Route)]
		public async Task<IActionResult> SubscribeByLcoations([FromBody] SubscribeByLocationsInstTaskDto model)
		{
			if (!ModelState.IsValid)
			{
				return BadRequest(ModelState);
			}

			PromotionTask promotionTask = _mapper.Map<SubscribeByLocationsInstTaskDto, PromotionTask>(model);

			if (IsPromotionTaskExists(promotionTask))
			{
				ApplicationUser user = await _userManager.GetUserAsync(User);
				if (!_db.SocialNetworkAccounts.Any(account => account.UserId == user.Id))
				{
					return BadRequest();
				}

				_db.PromotionTasks.Update(promotionTask);
			}
			else
			{
				_db.PromotionTasks.Add(promotionTask);
				try
				{
					_db.SaveChanges();
				}
				catch (DbUpdateException)
				{
					return BadRequest(ResultCode.InternalError);
				}

				if (!SaveToAccountPromotionTasks(model.SocialNetworkAccountId, promotionTask.PromotionTaskId))
				{
					return BadRequest(ResultCode.InternalError);
				}
			}

			try
			{
				_db.SaveChanges();
			}
			catch (DbUpdateException)
			{
				return BadRequest(ResultCode.InternalError);
			}


			var dataPreparator = new InstagramDataPreparator();

			RecurringJob.AddOrUpdate($"{promotionTask.PromotionTaskId}", () =>
				dataPreparator.SubscribeByGeoLocationDataPreparation(promotionTask.PromotionTaskId), Cron.MinuteInterval(1));

			return Ok();
		}


		[HttpPost]
		[Route(Constant.Controller.InstagramPromotion.LikeByLocataions.Route)]
		public async Task<IActionResult> LikeByLcoations([FromBody] LikeByLocationsInstTaskDto model)
		{
			if (!ModelState.IsValid)
			{
				return BadRequest(ModelState);
			}

			PromotionTask promotionTask = _mapper.Map<LikeByLocationsInstTaskDto, PromotionTask>(model);

			if (IsPromotionTaskExists(promotionTask))
			{
				ApplicationUser user = await _userManager.GetUserAsync(User);
				if (!_db.SocialNetworkAccounts.Any(account => account.UserId == user.Id))
				{
					return BadRequest();
				}

				_db.PromotionTasks.Update(promotionTask);
			}
			else
			{
				_db.PromotionTasks.Add(promotionTask);
				try
				{
					_db.SaveChanges();
				}
				catch (DbUpdateException)
				{
					return BadRequest(ResultCode.InternalError);
				}

				if (!SaveToAccountPromotionTasks(model.SocialNetworkAccountId, promotionTask.PromotionTaskId))
				{
					return BadRequest(ResultCode.InternalError);
				}
			}

			try
			{
				_db.SaveChanges();
			}
			catch (DbUpdateException)
			{
				return BadRequest(ResultCode.InternalError);
			}


			var dataPreparator = new InstagramDataPreparator();

			RecurringJob.AddOrUpdate($"{promotionTask.PromotionTaskId}", () =>
				dataPreparator.LikeByGeoLocationDataPreparation(promotionTask.PromotionTaskId), Cron.MinuteInterval(1));

			return Ok();
		}

		[HttpPost]
		[Route(Constant.Controller.InstagramPromotion.LikeFollowers.Route)]
		public async Task<IActionResult> LikeFollowers([FromBody] LikeFollowersTaskDto model)
		{
			if (!ModelState.IsValid)
			{
				return BadRequest(ModelState);
			}

			PromotionTask promotionTask = _mapper.Map<LikeFollowersTaskDto, PromotionTask>(model);

			if (IsPromotionTaskExists(promotionTask))
			{
				ApplicationUser user = await _userManager.GetUserAsync(User);
				if (!_db.SocialNetworkAccounts.Any(account => account.UserId == user.Id))
				{
					return BadRequest();
				}

				_db.PromotionTasks.Update(promotionTask);
			}
			else
			{
				_db.PromotionTasks.Add(promotionTask);
				try
				{
					_db.SaveChanges();
				}
				catch (DbUpdateException)
				{
					return BadRequest(ResultCode.InternalError);
				}

				if (!SaveToAccountPromotionTasks(model.SocialNetworkAccountId, promotionTask.PromotionTaskId))
				{
					return BadRequest(ResultCode.InternalError);
				}
			}

			try
			{
				_db.SaveChanges();
			}
			catch (DbUpdateException)
			{
				return BadRequest(ResultCode.InternalError);
			}


			var dataPreparator = new InstagramDataPreparator();

			RecurringJob.AddOrUpdate($"{promotionTask.PromotionTaskId}", () =>
				dataPreparator.LikeFollowersDataPreparation(promotionTask.PromotionTaskId), Cron.MinuteInterval(1));

			return Ok();
		}

		[HttpPost]
		[Route(Constant.Controller.InstagramPromotion.LikeByTag.Route)]
		public async Task<IActionResult> LikeByTag([FromBody] LikeByTagsInstTaskDto model)
		{
			if (!ModelState.IsValid)
			{
				return BadRequest(ModelState);
			}

			PromotionTask promotionTask = _mapper.Map<LikeByTagsInstTaskDto, PromotionTask>(model);

			if (IsPromotionTaskExists(promotionTask))
			{
				ApplicationUser user = await _userManager.GetUserAsync(User);
				if (!_db.SocialNetworkAccounts.Any(account => account.UserId == user.Id))
				{
					return BadRequest();
				}

				_db.PromotionTasks.Update(promotionTask);
			}
			else
			{
				_db.PromotionTasks.Add(promotionTask);
				try
				{
					_db.SaveChanges();
				}
				catch (DbUpdateException)
				{
					return BadRequest(ResultCode.InternalError);
				}

				if (!SaveToAccountPromotionTasks(model.SocialNetworkAccountId, promotionTask.PromotionTaskId))
				{
					return BadRequest(ResultCode.InternalError);
				}
			}

			try
			{
				_db.SaveChanges();
			}
			catch (DbUpdateException)
			{
				return BadRequest(ResultCode.InternalError);
			}


			var dataPreparator = new InstagramDataPreparator();

			RecurringJob.AddOrUpdate($"{promotionTask.PromotionTaskId}", () =>
				dataPreparator.LikeByTagDataPreparation(promotionTask.PromotionTaskId), Cron.MinuteInterval(1));

			return Ok();
		}

		private bool IsPromotionTaskExists(PromotionTask promotionTask)
		{
			uint? promotionTaskId = promotionTask?.PromotionTaskId;
			return promotionTaskId != null
			       && promotionTaskId != 0
			       && _db.PromotionTasks.Any(e => e.PromotionTaskId == promotionTaskId);
		}

		private bool SaveToAccountPromotionTasks(int accountId, uint promotionTaskId)
		{
			var task = new AccountPromotionTask
			{
				TaskId = promotionTaskId,
				SocialNetworkAccountId = accountId
			};

			if (_db.AccountPromotionTasks.Any(t => t == task))
			{
				return false;
			}

			_db.AccountPromotionTasks.Add(task);
			try
			{
				_db.SaveChanges();
			}
			catch (DbUpdateException)
			{
				return false;
			}

			return true;
		}
	}
}
