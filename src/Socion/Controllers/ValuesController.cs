﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using ConstantLibrary;

namespace Socion.Controllers
{
	[Route(Constant.Controller.Values.Route)]
	public class ValuesController : Controller
	{
		[Authorize]
		[HttpGet("claims")]
		public object Claims()
		{
			return User.Claims.Select(c =>
				new
				{
					c.Type,
					c.Value
				});
		}

		[HttpGet]
		[Authorize(Roles = Constant.Role.Admin)]
		public IEnumerable<string> Get()
		{
			// TODO: What the fuck?
			return new string[] { "value1", "value2" };
		}

		[HttpGet]
		[Route(Constant.Controller.Values.Get.Route)]
		public List<Claim> Get(int id)
		{
			return User.Claims.ToList();
		}

		[HttpPost]
		public void Post([FromBody]string value)
		{
			throw new NotImplementedException();
		}

		[HttpPut]
		[Route(Constant.Controller.Values.Put.Route)]
		public void Put(int id, [FromBody]string value)
		{
			throw new NotImplementedException();
		}

		[HttpDelete]
		[Route(Constant.Controller.Values.Delete.Route)]
		public void Delete(int id)
		{
			throw new NotImplementedException();
		}
	}
}
