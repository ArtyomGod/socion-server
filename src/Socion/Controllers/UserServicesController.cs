﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using ConstantLibrary;
using EntityFramework;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Identity;
using System.Threading.Tasks;

namespace Socion.Controllers
{
	[Produces(Constant.Controller.UserServices.Produces)]
	[Route(Constant.Controller.UserServices.Route)]	
	[Authorize]
	public class UserServicesController : Controller
	{
		private readonly SocionContext _db;
		
		public UserServicesController(SocionContext db)
		{
			_db = db;
		}

		private enum ResultCode
		{
			UserServiceAlreadyExist,
			InternalError
		}

		[HttpGet]
		[Authorize(Roles = Constant.Role.Admin)]
		[Route(Constant.Controller.UserServices.GetUsersServices.Route)]
		public IEnumerable<UserServices> GetUsersServices()
		{
			return _db.UserServices;
		}

		[HttpGet]		
		[Authorize(Roles = Constant.Role.Admin)]
		[Route(Constant.Controller.UserServices.GetUserServicesById.Route)]
		public IActionResult GetUserServicesById([FromRoute] int userId)
		{			
			if (!ModelState.IsValid)
			{
				return BadRequest(ModelState);
			}

			IQueryable<UserServices> userServices = _db.UserServices.Where(m => m.UserId == userId);

			if (!userServices.Any())
			{
				return NotFound();
			}

			return Ok(userServices);
		}

		[HttpGet]
		[Route(Constant.Controller.UserServices.GetUserServices.Route)]
		public IActionResult GetUserServices()
		{			
			int userId =  Convert.ToInt32(User.Claims.First(c => c.Type == ClaimTypes.NameIdentifier).Value);

			IQueryable<UserServices> userServices = _db.UserServices.Where(m => m.UserId == userId);

			if (!userServices.Any())
			{
				return NotFound();
			}

			return Ok(userServices);
		}

		[HttpPost]
		[Route(Constant.Controller.UserServices.AddUserService.Route)]
		public IActionResult AddUserService(int serviceId)
		{
			if (!ModelState.IsValid)
			{
				return BadRequest(ModelState);
			}

			var userServices = new UserServices
			{
				UserId = Convert.ToInt32(User.Claims.First(c => c.Type == ClaimTypes.NameIdentifier).Value),
				ServiceId = serviceId
			};

			if (UserServicesExists(userServices))
			{
				return BadRequest(ResultCode.UserServiceAlreadyExist);
			}

			_db.UserServices.Add(userServices);

			try
			{
				_db.SaveChanges();
			}
			catch (DbUpdateException)
			{
				return BadRequest(ResultCode.InternalError);
			}

			return Ok();
		}

		[HttpDelete]
		[Route(Constant.Controller.UserServices.DeleteUserService.Route)]
		public IActionResult DeleteUserService(int serviceId)
		{
			if (!ModelState.IsValid)
			{
				return BadRequest(ModelState);
			}

			int userId = Convert.ToInt32(User.Claims.First(c => c.Type == ClaimTypes.NameIdentifier).Value);

			var userServices = _db.UserServices.SingleOrDefault(m => m.ServiceId == serviceId && m.UserId == userId);
			if (userServices == null)
			{
				return NotFound();
			}

			_db.UserServices.Remove(userServices);
			_db.SaveChanges();

			return Ok(userServices);
		}

		private bool UserServicesExists(UserServices userService)
		{
			return userService != null && _db.UserServices.Any(e => e.ServiceId == userService.ServiceId && e.UserId == userService.UserId);
		}
	}
}
