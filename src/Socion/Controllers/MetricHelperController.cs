﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Hangfire;
using Hangfire.Dashboard;
using MetricHelperLibrary;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Socion.Controllers
{
    [Produces(ConstantLibrary.Constant.Controller.MetricHelperController.Produces)]
    [Route(ConstantLibrary.Constant.Controller.MetricHelperController.Route)]	
	[Authorize]
    public class MetricHelperController : Controller
    {
		[HttpGet(ConstantLibrary.Constant.Controller.MetricHelperController.RunLikeMetricFlush.Route)]
		public void RunLikeMetricFlush()
	    {
		    MetricHelper.FlushInstMetrics();
	    }
    
		[HttpGet(ConstantLibrary.Constant.Controller.MetricHelperController.AddJobMetricFlush.Route)]
		public void AddJobMetricFlush()
	    {
			RecurringJob.AddOrUpdate(ConstantLibrary.Constant.MetricHelperLibrary.JobName.FlushMetricInst, () => MetricHelper.FlushInstMetrics(), "0 1 * * *");
	    }
	    
		[HttpGet(ConstantLibrary.Constant.Controller.MetricHelperController.GetLikeMetricPerDay.Route)]
		public int GetLikeMetricPerDay(int id)
	    {
			return MetricHelper.GetLikeMetricInstagram(id);
	    }

		[HttpGet(ConstantLibrary.Constant.Controller.MetricHelperController.GetSubMetricPerDay.Route)]
		public int GetSubMetricPerDay(int id)
	    {
			return MetricHelper.GetSubMetricInstagram(id);
	    }

		[HttpGet(ConstantLibrary.Constant.Controller.MetricHelperController.GetUnsubMetricPerDay.Route)]
		public int GetUnsubMetricPerDay(int id)
	    {
			return MetricHelper.GetUnsubMetricInstagram(id);
	    }
	}
}