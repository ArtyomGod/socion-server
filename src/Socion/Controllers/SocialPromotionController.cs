﻿using System;
using ConstantLibrary;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Linq;
using System.Security.Claims;
using EntityFramework;
using Hangfire;
using System.Collections.Generic;
using Socion.DataPreparator;
using Socion.Dto;
using Socion.Helper;


namespace Socion.Controllers
{
	[Produces(Constant.Controller.SocialPromotion.Produces)]
	[Route(Constant.Controller.SocialPromotion.Route)]
	[Authorize]
	public class SocialPromotionController : Controller
	{
		private readonly SocionContext _db;

		public SocialPromotionController(SocionContext context)
		{
			_db = context;
		}

		[HttpGet]
		[Route(Constant.Controller.SocialPromotion.GetPromotionTask.Route)]
		public IActionResult GetPromotionTask(int promotionTaskId)
		{
			int userId = Convert.ToInt32(User.Claims.First(c => c.Type == ClaimTypes.NameIdentifier).Value);
			var userAccountsInSocialNetwork = _db.SocialNetworkAccounts.Where(a => a.UserId == userId).ToList();
			var isPromotionTaskBelongsToUser = _db.AccountPromotionTasks.Any(a =>
				userAccountsInSocialNetwork.Any(x => x.SocialNetworkAccountId == a.SocialNetworkAccountId)
				&& a.TaskId == promotionTaskId);

			if (!isPromotionTaskBelongsToUser)
			{
				return NotFound();
			}

			var accountPromotionTasks = _db.AccountPromotionTasks.FirstOrDefault(a => a.TaskId == promotionTaskId);
			var promotionTask = _db.PromotionTasks.FirstOrDefault(task => task.PromotionTaskId == promotionTaskId);

			IPromotionTaskDto promotionTaskDto = PromotionTaskConverter.GetDtoFromEntity(promotionTask);
			promotionTaskDto.SocialNetworkAccountId = accountPromotionTasks.SocialNetworkAccountId;

			return Ok(promotionTaskDto);
		}

		[HttpPost]
		[Route(Constant.Controller.SocialPromotion.ChangeRecurringPeriodOfTask.Route)]
		public IActionResult ChangeRecurringPeriodOfTask([FromBody] PromotionTaskSettingsDto promotionTaskSettings)
		{
			if (!ModelState.IsValid)
			{
				return BadRequest(ModelState);
			}
			InstagramDataPreparator dataPreparator = new InstagramDataPreparator();
			var promotionTaskType = _db.PromotionTasks.Where(a => a.PromotionTaskId== promotionTaskSettings.PromotionTaskId).Select(task => task.PromotionTaskType).ToList().FirstOrDefault();
			switch (promotionTaskType)
			{
				case PromotionTaskType.LikeByTags:
					RecurringJob.AddOrUpdate($"{promotionTaskSettings.PromotionTaskId}", () => dataPreparator.LikeByTagDataPreparation((uint)promotionTaskSettings.PromotionTaskId), promotionTaskSettings.Cron);
					break;
				case PromotionTaskType.LikeFollowers:
					RecurringJob.AddOrUpdate($"{promotionTaskSettings.PromotionTaskId}", () => dataPreparator.LikeFollowersDataPreparation((uint)promotionTaskSettings.PromotionTaskId), promotionTaskSettings.Cron);
					break;
				case PromotionTaskType.SubscribeByCompetitor:
					RecurringJob.AddOrUpdate($"{promotionTaskSettings.PromotionTaskId}", () => dataPreparator.SubscribeByCompetitorDataPreparation((uint)promotionTaskSettings.PromotionTaskId), promotionTaskSettings.Cron);
					break;
				case PromotionTaskType.SubscribeByTagInstagram:
					RecurringJob.AddOrUpdate($"{promotionTaskSettings.PromotionTaskId}", () => dataPreparator.SubscribeByTagDataPreparation((uint)promotionTaskSettings.PromotionTaskId), promotionTaskSettings.Cron);
					break;					
				case PromotionTaskType.SubscribeByLocationsInstagram:
					RecurringJob.AddOrUpdate($"{promotionTaskSettings.PromotionTaskId}", () => dataPreparator.SubscribeByGeoLocationDataPreparation((uint)promotionTaskSettings.PromotionTaskId), promotionTaskSettings.Cron);
					break;					
				case PromotionTaskType.LikeByLocationsInstagram:
					RecurringJob.AddOrUpdate($"{promotionTaskSettings.PromotionTaskId}", () => dataPreparator.LikeByGeoLocationDataPreparation((uint)promotionTaskSettings.PromotionTaskId), promotionTaskSettings.Cron);
					break;
			}

			return Ok();
		}

		[HttpGet]
		[Route(Constant.Controller.SocialPromotion.GetAllUserPromotionTasks.Route)]
		public IActionResult GetAllUserPromotionTasks()
		{
			int userId = Convert.ToInt32(User.Claims.First(c => c.Type == ClaimTypes.NameIdentifier).Value);
			var userAccountsInSocialNetwork = _db.SocialNetworkAccounts.Where(a => a.UserId == userId).ToList();
			var accountPromotionTasks = _db.AccountPromotionTasks.Where(a => userAccountsInSocialNetwork.Any(
				x => x.SocialNetworkAccountId == a.SocialNetworkAccountId)).ToList();

			List<IPromotionTaskDto> promotionTasksDto = new List<IPromotionTaskDto>();
			var promotionTasks = _db.PromotionTasks.Where(a => accountPromotionTasks.Any(
				x => x.TaskId == a.PromotionTaskId)).ToList();

			for (int i = 0; i < accountPromotionTasks.Count; ++i)
			{
				IPromotionTaskDto promotionTaskDto = PromotionTaskConverter.GetDtoFromEntity(promotionTasks[i]);
				promotionTaskDto.SocialNetworkAccountId = accountPromotionTasks[i].SocialNetworkAccountId;
				promotionTasksDto.Add(promotionTaskDto);
			}

			return Ok(promotionTasksDto);
		}

		[HttpGet]
		[Route(Constant.Controller.SocialPromotion.GetUserPromotionTasksBySocialNetworkId.Route)]
		public IActionResult GetUserPromotionTasksBySocialNetworkId(int socialNetworkId)
		{
			int userId = Convert.ToInt32(User.Claims.First(c => c.Type == ClaimTypes.NameIdentifier).Value);
			var userAccountsInSocialNetwork = _db.SocialNetworkAccounts.Where(a =>
				a.UserId == userId && a.SocialNetworkId == socialNetworkId).ToList();

			var accountPromotionTasks = _db.AccountPromotionTasks.Where(a => userAccountsInSocialNetwork.Any(
				x => x.SocialNetworkAccountId == a.SocialNetworkAccountId)).ToList();

			var promotionTasks = _db.PromotionTasks.Where(a => accountPromotionTasks.Any(
				x => x.TaskId == a.PromotionTaskId)).ToList();

			List<IPromotionTaskDto> promotionTasksDto = new List<IPromotionTaskDto>();

			for (int i = 0; i < accountPromotionTasks.Count; ++i)
			{
				IPromotionTaskDto promotionTaskDto = PromotionTaskConverter.GetDtoFromEntity(promotionTasks[i]);
				promotionTaskDto.SocialNetworkAccountId = accountPromotionTasks[i].SocialNetworkAccountId;
				promotionTasksDto.Add(promotionTaskDto);
			}

			return Ok(promotionTasksDto);
		}

		[HttpDelete]
		[Route(Constant.Controller.SocialPromotion.Delete.Route)]
		public IActionResult Delete(int taskId)
		{
			int userId = Convert.ToInt32(User.Claims.First(c => c.Type == ClaimTypes.NameIdentifier).Value);
			SocialNetworkAccount socialNetworkAccount = _db.SocialNetworkAccounts.FirstOrDefault(a => a.UserId == userId);
			if (socialNetworkAccount == null)
			{
				return BadRequest();
			}

			if (!_db.AccountPromotionTasks.Any(a => a.TaskId == taskId && a.SocialNetworkAccountId == socialNetworkAccount.SocialNetworkAccountId))
			{
				return NotFound();
			}

			RecurringJob.RemoveIfExists($"{taskId}");

			AccountPromotionTask accountPromotionTasksToDelete = _db.AccountPromotionTasks.FirstOrDefault(p => p.TaskId == taskId);
			_db.AccountPromotionTasks.Remove(accountPromotionTasksToDelete);

			PromotionTask promotionTaskToDelete = _db.PromotionTasks.FirstOrDefault(p => p.PromotionTaskId == taskId);
			_db.PromotionTasks.Remove(promotionTaskToDelete);

			try
			{
				_db.SaveChanges();
			}
			catch (System.Exception e)
			{
				throw e;
			}

			return Ok();
		}
	}
}
