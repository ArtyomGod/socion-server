﻿using System.Linq;
using System.Threading.Tasks;
using EntityFramework;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Socion.Dto;
using ConstantLibrary;

namespace Socion.Controllers
{
	[Produces(Constant.Controller.UserSettings.Produces)]
	[Route(Constant.Controller.UserSettings.Route)]
	public class UserSettingsController : Controller
	{
		private readonly UserManager<ApplicationUser> _userManager;

		public UserSettingsController(UserManager<ApplicationUser> userManager)
		{
			_userManager = userManager;
		}

		private enum ResultCode
		{
			UserNotFound,
			EmailAlreadyExist
		}

		[HttpGet]
		public async Task<IActionResult> Get()
		{
			ApplicationUser user = await _userManager.GetUserAsync(User);
			if (user == null)
			{
				return BadRequest(ResultCode.UserNotFound);
			}

			UserSettingsDto userSettingsDto = new UserSettingsDto
			{
				Email = user.Email,
				EmailConfirmed = user.EmailConfirmed
			};

			return Ok(userSettingsDto);
		}

		[HttpPost]
		public async Task<IActionResult> Post([FromBody] UserSettingsDto userSettingsDto)
		{
			if (!ModelState.IsValid)
			{
				return BadRequest(ModelState);
			}

			Task<ApplicationUser> gettingUserTask = _userManager.GetUserAsync(User);
			gettingUserTask.Wait();

			ApplicationUser user = gettingUserTask.Result;

			if (user == null)
			{
				return BadRequest(ResultCode.UserNotFound);
			}

			if (userSettingsDto.Email != null)
			{
				if (IsEmailExist(userSettingsDto.Email))
				{
					return BadRequest(ResultCode.EmailAlreadyExist);
				}

				user.NewEmail = userSettingsDto.Email;
				user.EmailConfirmed = false;

				await _userManager.UpdateAsync(user);
			}

			if (IsPasswordsNotNull(userSettingsDto))
			{
				IdentityResult identityResult = await _userManager.ChangePasswordAsync(user, userSettingsDto.CurrentPassword, userSettingsDto.NewPassword);
				if (!identityResult.Succeeded)
				{
					return BadRequest(identityResult.Errors);
				}
			}
			Helper.EmailSenderHelper.Instance.SendConfirmationEmail(user, _userManager);

			return Ok(gettingUserTask);
		}

		private bool IsPasswordsNotNull(UserSettingsDto userSettingsDto)
		{
			return userSettingsDto.CurrentPassword != null && userSettingsDto.NewPassword != null;
		}

		private bool IsEmailExist(string email)
		{
			return _userManager.Users.Any(userApp => userApp.Email == email && userApp.NewEmail == email);
		}
	}
}
