﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using EntityFramework;
using Microsoft.AspNetCore.Authorization;
using Socion.Dto;

namespace Socion.Controllers
{
	[Produces(ConstantLibrary.Constant.Controller.ProxiesManagerController.Produces)]
	[Route(ConstantLibrary.Constant.Controller.ProxiesManagerController.Route)]
	[Authorize]
	public class ProxiesManagerController : Controller
	{
		private readonly SocionContext _context;
		private readonly IMapper _mapper;

		public ProxiesManagerController(SocionContext context, IMapper mapper)
		{
			_context = context;
			_mapper = mapper;
		}
		
		public IActionResult GetUserProxies()
		{
			int userId = Convert.ToInt32(User.Claims.First(c => c.Type == ClaimTypes.NameIdentifier).Value);

			if (!ModelState.IsValid)
			{
				return BadRequest(ModelState);
			}

			var userProxies = _context.UserProxies.Where(m => m.UserId == userId);
			List<ProxyDto> proxyDtos = new List<ProxyDto>();

			foreach (var userProxy in userProxies)
			{
				Proxy proxy = _context.Proxies.SingleOrDefault(p => p.ProxyId == userProxy.ProxyId);

				proxyDtos.Add(_mapper.Map<Proxy, ProxyDto>(proxy));
			}

			return Ok(proxyDtos);
		}

		[HttpGet("{id}")]
		public IActionResult GetProxy(int id)
		{
			int userId = Convert.ToInt32(User.Claims.First(c => c.Type == ClaimTypes.NameIdentifier).Value);

			if (!ModelState.IsValid)
			{
				return BadRequest(ModelState);
			}
			var isProxyBelongsToUser = _context.UserProxies.Any(m => m.UserId == userId && m.ProxyId == id);
			if (isProxyBelongsToUser)
			{
				Proxy proxy = _context.Proxies.SingleOrDefault(p => p.ProxyId == id);

				return proxy != null ? (IActionResult) Ok(_mapper.Map<Proxy, ProxyDto>(proxy)) : BadRequest();
			}
			return BadRequest();
		}
		
		[HttpPost]
		public async Task<IActionResult> PostProxy([FromBody] ProxyDto proxyDto)
		{
			if (!ModelState.IsValid)
			{
				return BadRequest(ModelState);
			}
			Proxy proxy = _mapper.Map<ProxyDto, Proxy>(proxyDto);
			if (proxy.ProxyId != 0)
			{
				_context.Proxies.Update(proxy);
			}
			else
			{
				_context.Proxies.Add(proxy);
				await _context.SaveChangesAsync();
				int userId = Convert.ToInt32(User.Claims.First(c => c.Type == ClaimTypes.NameIdentifier).Value);
				UserProxy userProxy = new UserProxy
				{
					ProxyId = proxy.ProxyId,
					UserId = userId
				};
				_context.UserProxies.Add(userProxy);
			}
			await _context.SaveChangesAsync();

			return Ok();
		}

		[HttpPost("bind-proxy-to-account")]
		public async Task<IActionResult> BindProxyToSocialNetworkAccount([FromBody] SocialNetworkAccountProxyDto socialNetworkAccountProxyDto)
		{
			if (!ModelState.IsValid)
			{
				return BadRequest(ModelState);
			}
			int userId = Convert.ToInt32(User.Claims.First(c => c.Type == ClaimTypes.NameIdentifier).Value);
			var socialNetworkAccountProxy = _mapper.Map<SocialNetworkAccountProxyDto, SocialNetworkAccountProxy>(socialNetworkAccountProxyDto);

			bool isAccountBelongsToUser = _context.SocialNetworkAccounts.Any(a =>
				a.UserId == userId && a.SocialNetworkAccountId == socialNetworkAccountProxy.SocialNetworkAccountId);
			bool isProxyBelongsToUser = _context.UserProxies.Any(a => a.ProxyId == socialNetworkAccountProxyDto.ProxyId && a.UserId == userId);
			if (!isAccountBelongsToUser || !isProxyBelongsToUser)
			{
				return BadRequest();
			}

			bool isExistSocialAccount =
				_context.SocialNetworkAccountProxies.Any(s => s.SocialNetworkAccountId == socialNetworkAccountProxyDto.SocialNetworkAccountId);
			if (isExistSocialAccount)
			{
				var toChangeAccountProxy = _context.SocialNetworkAccountProxies.SingleOrDefault(s =>
					s.SocialNetworkAccountId == socialNetworkAccountProxy.SocialNetworkAccountId);
				if (toChangeAccountProxy == null)
				{
					return BadRequest();
				}
				toChangeAccountProxy.ProxyId = socialNetworkAccountProxy.ProxyId;
				_context.SocialNetworkAccountProxies.Update(toChangeAccountProxy);
			}
			else
			{
				_context.SocialNetworkAccountProxies.Add(socialNetworkAccountProxy);
			}
			await _context.SaveChangesAsync();

			return Ok(_context.SocialNetworkAccountProxies);
		}

		[HttpDelete("{id}")]
		public async Task<IActionResult> DeleteProxy([FromRoute] int id)
		{
			if (!ModelState.IsValid)
			{
				return BadRequest(ModelState);
			}

			var proxy = await _context.Proxies.SingleOrDefaultAsync(m => m.ProxyId == id);
			if (proxy == null)
			{
				return NotFound();
			}

			_context.Proxies.Remove(proxy);
			await _context.SaveChangesAsync();

			return Ok(proxy);
		}
	}
}