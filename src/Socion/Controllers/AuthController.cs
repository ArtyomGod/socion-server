﻿using System.Linq;
using System.Threading.Tasks;
using ConstantLibrary;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Socion.Dto;
using EntityFramework;
using Socion.Secure;

namespace Socion.Controllers
{
	[Route(Constant.Controller.Auth.Route)]
	public class AuthController : Controller
	{
		private readonly IConfiguration _configuration;
		private readonly UserManager<ApplicationUser> _userManager;
		private readonly SignInManager<ApplicationUser> _signInManager;

		public AuthController(
			IConfiguration configuration,
			UserManager<ApplicationUser> userManager,
			SignInManager<ApplicationUser> signInManager)
		{
			_configuration = configuration;
			_userManager = userManager;
			_signInManager = signInManager;
		}

		private enum ResultCode
		{
			InvalidLoginAttempt,
			InvalidModel,
			EmailAlreadyConfirmed,
			InvalidPassword,
			EmailIsBusy,
		}

		[HttpPost]
		[Route(Constant.Controller.Auth.SignIn.Route)]
		public async Task<IActionResult> Login([FromBody] AuthUserDto model)
		{
			var result = await _signInManager.PasswordSignInAsync(model.Email, model.Password, false, false);

			if (!result.Succeeded)
			{
				return BadRequest(ResultCode.InvalidLoginAttempt);
			}

			var user = _userManager.Users.SingleOrDefault(r => r.Email == model.Email);
			return Ok(JwtTokenHelper.Generate(model.Email, user, _configuration, _userManager));
		}

		[HttpPost]
		[Route(Constant.Controller.Auth.SignUp.Route)]
		public async Task<IActionResult> Register([FromBody] AuthUserDto model)
		{
			if (!ModelState.IsValid)
			{
				return BadRequest(ResultCode.InvalidModel);
			}

			var user = new ApplicationUser
			{
				UserName = model.Username ?? model.Email,
				Email = model.Email
			};

			{
				IdentityResult result = await _userManager.CreateAsync(user, model.Password);
				if (!result.Succeeded)
				{
					return BadRequest(result.Errors);
				}
			}

			{
				IdentityResult result = await _userManager.AddToRoleAsync(user, Constant.Role.CommonUser);
				if (!result.Succeeded)
				{
					return BadRequest(result.Errors);
				}
			}

			await _signInManager.SignInAsync(user, false);
			
			var confirmationTokenTask = _userManager.GenerateEmailConfirmationTokenAsync(user);
			confirmationTokenTask.Wait();
			
			Helper.EmailSenderHelper.Instance.SendConfirmationEmail(user, _userManager);

			return Ok(JwtTokenHelper.Generate(model.Email, user, _configuration, _userManager));
		}

		[HttpGet]
		[Route(Constant.Controller.Auth.Confirmation.Route)]
		public async Task<IActionResult> Confirmation(int? userId, string token)
		{
			if (userId == null || token == null)
			{
				return BadRequest();
			}

			ApplicationUser user = await _userManager.FindByIdAsync(userId.ToString());

			if (user == null)
			{
				return BadRequest();
			}

			if (user.Email != null && user.NewEmail != null)
			{
				user.Email = user.NewEmail;
				//user.UserName = user.NewEmail;
				user.NewEmail = null;
			}

			IdentityResult result = await _userManager.ConfirmEmailAsync(user, token);
			if (!result.Succeeded)
			{
				return NotFound(result.Errors);
			}

			return Ok();
		}

		[HttpGet]
		[Route(Constant.Controller.Auth.SendConfirmation.Route)]
		public async Task<IActionResult> SendConfirmation()
		{
			ApplicationUser user = await _userManager.GetUserAsync(User);
			if (await _userManager.IsEmailConfirmedAsync(user))
			{
				return Ok(ResultCode.EmailAlreadyConfirmed);
			}

			var confirmationTokenTask = _userManager.GenerateEmailConfirmationTokenAsync(user);
			confirmationTokenTask.Wait();
			
			Helper.EmailSenderHelper.Instance.SendConfirmationEmail(user, _userManager);

			return Ok();
		}
		
	}
}
