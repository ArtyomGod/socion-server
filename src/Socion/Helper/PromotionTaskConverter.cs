﻿using System.ComponentModel;
using AutoMapper;
using ConstantLibrary;
using Socion.Dto;
using EntityFramework;

namespace Socion.Helper
{
    public class PromotionTaskConverter
    {
	    public static IPromotionTaskDto GetDtoFromEntity(PromotionTask promotionTask)
	    {
		    if (promotionTask == null)
		    {
			    return null;
		    }

			return DetermineTaskType(promotionTask);
		    
		}

	    private static IPromotionTaskDto DetermineTaskType(PromotionTask promotionTask)
	    {
		    switch (promotionTask.PromotionTaskType)
		    {
				case PromotionTaskType.SubscribeByTagInstagram:
					return Mapper.Map<PromotionTask, SubscribeByTagsInstTaskDto>(promotionTask);

				case PromotionTaskType.SubscribeByCompetitor:
					return Mapper.Map<PromotionTask, SubscribeByCompetitorInstTaskDto>(promotionTask);

			}

		    throw new InvalidEnumArgumentException();
	    }
	}
}
