﻿using System;
using System.Threading.Tasks;
using System.Web;
using ConstantLibrary;
using EntityFramework;
using Microsoft.AspNetCore.Identity;
using Model;
using Newtonsoft.Json;
using RabbitMqLibrary;
using RedisLibrary;

namespace Socion.Helper
{
	public class EmailSenderHelper
	{
		private readonly RabbitMq _rabbitMq = new RabbitMq(); 

		public static readonly EmailSenderHelper Instance = new EmailSenderHelper();

		public void SendConfirmationEmail(ApplicationUser user, UserManager<ApplicationUser> userManager)
		{
			Task<string> confirmationTokenTask = userManager.GenerateEmailConfirmationTokenAsync(user);
			confirmationTokenTask.Wait();

			var callbackurl = $"http://localhost:5000/auth/confirmation?userId={user.Id}&token={HttpUtility.UrlEncode(confirmationTokenTask.Result)}";
			var emailBody = string.Format(Constant.EmailSender.EmailUpdating.Message, callbackurl);

			var emailInformation = new EmailInformation
			{
				FromAddress = Constant.EmailSender.FromAddress,
				FromName = Constant.EmailSender.FromName,
				ToAddress = user.NewEmail ?? user.Email,
				Subject = Constant.EmailSender.EmailConfirmation.Subject,
				Body = emailBody
			};

			string emailId = Guid.NewGuid().ToString();
			string redisKey = $"{Constant.Redis.Prefix.Email}{emailId}";

			Redis.Instance.Database.StringSet(redisKey, JsonConvert.SerializeObject(emailInformation));

			_rabbitMq.PublishToExchange(Constant.RabbitMq.ExchangeName.Email, emailId);
		}

		private EmailSenderHelper()
		{

		}
	}
}
