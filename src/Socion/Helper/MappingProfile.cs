using System;
using AutoMapper;
using Socion.Dto;
using EntityFramework;

namespace Socion.Helper
{
	public class MappingProfile : Profile
	{
		public MappingProfile()
		{
			CreateMap<ApplicationUser, AuthUserDto>();
			CreateMap<AuthUserDto, ApplicationUser>();
			CreateMap<SubscribeByTagsInstTaskDto, PromotionTask>();
			CreateMap<PromotionTask, SubscribeByTagsInstTaskDto>();
			CreateMap<SubscribeByLocationsInstTaskDto, PromotionTask>();
			CreateMap<PromotionTask, SubscribeByLocationsInstTaskDto>();
			CreateMap<LikeByLocationsInstTaskDto, PromotionTask>();
			CreateMap<PromotionTask, LikeByLocationsInstTaskDto>();
			CreateMap<LikeFollowersTaskDto, PromotionTask>();
			CreateMap<PromotionTask, LikeFollowersTaskDto>();
			CreateMap<LikeByTagsInstTaskDto, PromotionTask>();
			CreateMap<PromotionTask, LikeByTagsInstTaskDto>();
			CreateMap<SubscribeByCompetitorInstTaskDto, PromotionTask>();
			CreateMap<PromotionTask, SubscribeByCompetitorInstTaskDto>();
			CreateMap<SocialNetworkAccountProxyDto, SocialNetworkAccountProxy>();
			CreateMap<SocialNetworkAccountProxy, SocialNetworkAccountProxyDto>();

			CreateMap<SocialNetworkAccount, SocialNetworkAccountDto>().ForMember(t => t.TokenLifetime,
				opt => opt.MapFrom(
					src => src.TokenLifetime.Value.Millisecond
				)
			);

			CreateMap<SocialNetworkAccountDto, SocialNetworkAccount>().ForMember(t => t.TokenLifetime,
				opt => opt.MapFrom(src => DateTime.Now.AddMilliseconds((long) src.TokenLifetime)
				)
			);

			CreateMap<Proxy, ProxyDto>().ForMember(t => t.ExpirationDate,
				opt => opt.MapFrom(
					src => src.ExpirationDate.Millisecond
				)
			);

			CreateMap<ProxyDto, Proxy>().ForMember(t => t.ExpirationDate,
				opt => opt.MapFrom(src => DateTime.Now.AddMilliseconds((long) src.ExpirationDate)
				)
			);
		}
	}
}
