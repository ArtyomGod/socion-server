﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.NetworkInformation;
using System.Threading.Tasks;
using ConstantLibrary;
using EntityFramework;

namespace Socion.Helper
{
    public class ProxyHelper
    {
	    public static async Task<bool> CanPing(Proxy proxy)
	    {
		    var httpHndler = new HttpClientHandler
		    {
			    Proxy = new WebProxy(proxy.Value, false)
			    {
				    UseDefaultCredentials = false,
				    Credentials = new NetworkCredential(
					    proxy.Login,
					    proxy.Password
					)
			    }
		    };

			using (var client = new HttpClient(httpHndler))
		    {
			    try
			    {
				    HttpResponseMessage response = await client.GetAsync(Constant.ProxyHelper.AddresToCheckProxyResponseStatus);
				    if (response.IsSuccessStatusCode)
				    {
					    return true;
				    }
			    }
			    catch
				{
				    return false;
			    }
			}

		    return false;
		}
		public static void PickUpNewProxyForAccount(int socialNetworkAccountId)
	    {
		    //найти прокси кол-во использование которых не больше 5
			//забиндить новую проксю к акку
	    }
	}
}
