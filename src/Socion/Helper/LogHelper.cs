﻿using System;
using System.Text;
using RabbitMqLibrary;
using RedisLibrary;
using ConstantLibrary;

namespace Socion.Helper
{
	public class LogHelper
	{
		private readonly RabbitMq _rabbitMq = new RabbitMq(); 

		public static readonly LogHelper Instance = new LogHelper();

		public void AddLog(string[] fields)
		{
			var logId = Guid.NewGuid().ToString();
			string redisKey = $"{Constant.Redis.Prefix.Log}{logId}";
			Redis.Instance.Database.StringSet(redisKey, Serialize(fields));
			_rabbitMq.PublishToExchange(Constant.RabbitMq.ExchangeName.Log, logId);
		}

		private LogHelper()
		{

		}

		private string Serialize(string[] fields)
		{
			var result = new StringBuilder();
			result.Append(new DateTimeOffset(DateTime.Now).ToUnixTimeMilliseconds());
			result.Append(Constant.Redis.Delimiter);
			
			return result.AppendJoin(Constant.Redis.Delimiter, fields).ToString();
		}
	}
}
