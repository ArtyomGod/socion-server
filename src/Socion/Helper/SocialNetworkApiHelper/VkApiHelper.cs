﻿using System;
using System.Collections.Generic;
using EntityFramework;
using VkNet;
using VkNet.Enums;
using VkNet.Enums.Filters;

namespace Socion.Helper.SocialNetworkApiHelper
{
	public class VkApiHelper : IApiHelper
	{
		private readonly VkApi _vkApi = new VkApi();

		public static readonly VkApiHelper Instance = new VkApiHelper();

		public void Authorize(SocialNetworkAccount account)
		{
			if (account == null)
			{
				throw new ArgumentNullException("Social network account wasn't found");
			}

			_vkApi.Authorize(new ApiAuthParams
			{
				AccessToken = account.AccessToken,
				Settings = Settings.All
			});

			if (!_vkApi.IsAuthorized)
			{
				throw new Exception("Authorization failed");
			}
		}

		public bool Subscribe(long userId)
		{
			return (_vkApi.Friends.Add(userId) & AddFriendStatus.Sended) != 0;
		}

		public bool Subscribe(IReadOnlyCollection<long> userIds)
		{
			foreach (long userId in userIds)
			{
				if (!Subscribe(userId))
				{
					return false;
				}
			}

			return true;
		}

		public bool Unsubscribe(long userId)
		{
			return _vkApi.Friends.Delete(userId).Success != null;
		}

		public bool Unsubscribe(IReadOnlyCollection<long> userIds)
		{
			foreach (long userId in userIds)
			{
				if (!Unsubscribe(userId))
				{
					return false;
				}
			}

			return true;
		}

		private VkApiHelper()
		{

		}
	}
}
