using System.Collections.Generic;
using EntityFramework;

namespace Socion.Helper.SocialNetworkApiHelper
{
	public interface IApiHelper
	{
		void Authorize(SocialNetworkAccount account);
		bool Subscribe(long userId);
		bool Subscribe(IReadOnlyCollection<long> userIds);
		bool Unsubscribe(long userId);
		bool Unsubscribe(IReadOnlyCollection<long> userIds);
	}
}
