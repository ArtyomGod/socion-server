﻿using System.Collections.Generic;
using Google.Protobuf.WellKnownTypes;
using InstaSharper.Classes.Models;
using Model;

namespace Socion.Helper.SocialNetworkApiHelper.InstagramApiHelper
{
	public interface IInstagramApiHelper : IApiHelper
	{
		PaginationUserInfo FindUserByTag(string tag, string startFromId);
		PaginationUserInfo GetUserFollowers(string username, string startFromId, int pageToLoad = ConstantLibrary.Constant.InstagramApiHelper.DefaultPageToLoadForPagination);
		PaginationUserInfo GetUserFollowers(string startFromId, int pageToLoad = 1);
		//void LikeAmmountOfMedia(string username, int ammountToLike = 1, int pageToLoad = 1, string startFromId = "");
		bool LikeAmmountOfMedia(string username, int ammountToLike = 1, int pageToLoad = 1, string startFromId = "");
		List<string> GetCurrentUserFollowersList(string startFromId, int pageToLoad = 1);
		List<string> GetUserFollowersList(string username, string startFromId, int pageToLoad = 1);
		bool IsFollowedByUser(long userId);
		bool IsFollowedByUser(string userName);
		InstaCurrentUser GetCurrentUser();
		InstaUser GetUser(string username);
		bool IsUserAuthenticated();
		InstaLocationShortList GetGeoTagsByLatLong(double latitude, double longitude, string query = "square");
		string GetGeoTagIdsAsString(InstaLocationShortList locationsList);
		InstaLocationFeed GetLocationFeed(string locationId, int pageToLoad = 1);
		string GetUserPksByLocationAsString(InstaLocationFeed locationsFeed, int maxFeedSize = 30);
		string GetUsernamesByLocationAsString(InstaLocationFeed locationsFeed, int maxFeedSize = 30);
	}
}
