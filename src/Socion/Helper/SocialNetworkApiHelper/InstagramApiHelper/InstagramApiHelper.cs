﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using InstaSharper.API;
using InstaSharper.API.Builder;
using InstaSharper.Classes;
using InstaSharper.Classes.Models;
using Model;
using EntityFramework;
using Microsoft.Rest.Azure;
using Socion.Dto;

namespace Socion.Helper.SocialNetworkApiHelper.InstagramApiHelper
{
	public class InstagramApiHelper : IInstagramApiHelper
	{
		private readonly IInstaApi _instaApi;
		
		public InstagramApiHelper(SocialNetworkAccount account, Proxy proxy)
		{
			if (account == null)
			{
				throw new ArgumentNullException("Social network account is null");
			}

			var userSession = new UserSessionData
			{
				UserName = account.Login,
				Password = account.Password
			};

			var httpHndler = new HttpClientHandler
			{
				Proxy = new WebProxy(proxy.Value, false)
				{
					UseDefaultCredentials = false,
					Credentials = new NetworkCredential(
						proxy.Login,
						proxy.Password
					)
				}
			};
			_instaApi = InstaApiBuilder.CreateBuilder()
				.SetUser(userSession)
				.SetRequestDelay(TimeSpan.FromMilliseconds(500))
				.UseHttpClientHandler(httpHndler)
				.Build();

			string stateFile = $"{ConstantLibrary.Constant.InstagramApiHelper.StateFilePath}{ConstantLibrary.Constant.InstagramApiHelper.StateFileName}{account.SocialNetworkAccountId}{ConstantLibrary.Constant.InstagramApiHelper.Dot}{ConstantLibrary.Constant.InstagramApiHelper.FileType}";
			try
			{
				if (File.Exists(stateFile))
				{
					Console.WriteLine("Loading state from file");
					using (var fs = File.OpenRead(stateFile))
					{
						_instaApi.LoadStateDataFromStream(fs);
					}
				}
			}
			catch (Exception e)
			{
				Console.WriteLine(e);
			}

			if (!_instaApi.IsUserAuthenticated)
			{
				Console.WriteLine($"Logging in as {userSession.UserName}");
				IResult<InstaLoginResult> loginResult =
					WaitForResult(() => _instaApi.LoginAsync());
				if (!loginResult.Succeeded)
				{
					Console.WriteLine($"Unable to login: {loginResult.Info.Message}");
					return;
				}
			}
			var state = _instaApi.GetStateDataAsStream();
			using (var fileStream = File.Create(stateFile))
			{
				state.Seek(0, SeekOrigin.Begin);
				state.CopyTo(fileStream);
			}
		}

		public void Authorize(SocialNetworkAccount account)
		{
			throw new NotImplementedException();
		}

		public bool Subscribe(IReadOnlyCollection<long> userIds)
		{
			foreach (long userId in userIds)
			{
				if (!Subscribe(userId))
				{
					return false;
				}
			}

			return true;
		}

		public bool Subscribe(string username)
		{
			IResult<InstaUser> user =
				WaitForResult(() => _instaApi.GetUserAsync(username));

			return Subscribe(user.Value.Pk);
		}

		public bool Subscribe(long userId)
		{
			if (IsFollowedByUser(userId))
			{
				return false;
			}
			IResult<InstaFriendshipStatus> instaFriendshipStatus =
				WaitForResult(() => _instaApi.FollowUserAsync(userId));

			return instaFriendshipStatus.Succeeded;
		}

		public bool Unsubscribe(long userId)
		{
			if (IsFollowedByUser(userId))
			{
				return true;
			}
			IResult<InstaFriendshipStatus> instaFriendshipStatus =
				WaitForResult(() => _instaApi.UnFollowUserAsync(userId));

			return instaFriendshipStatus.Succeeded;
		}

		public bool Unsubscribe(IReadOnlyCollection<long> userIds)
		{
			foreach (long userId in userIds)
			{
				if (!Unsubscribe(userId))
				{
					return false;
				}
			}

			return true;
		}

		public PaginationUserInfo FindUserByTag(string tag, string startFromId)
		{
			PaginationParameters paginationParameters = PaginationParameters.MaxPagesToLoad(1);
			if (startFromId != string.Empty)
			{
				paginationParameters = paginationParameters.StartFromId(startFromId);
			}

			IResult<InstaTagFeed> tagFeed = WaitForResult(() => _instaApi.GetTagFeedAsync(tag, paginationParameters));

			InstaUser user = tagFeed.Value.Medias.FirstOrDefault()?.User;

			return new PaginationUserInfo(user, paginationParameters.NextId);
		}

		public PaginationUserInfo GetUserFollowers(string username, string startFromId, int pageToLoad = 1)
		{
			PaginationParameters paginationParameters = PaginationParameters.MaxPagesToLoad(pageToLoad);
			if (startFromId != string.Empty)
			{
				paginationParameters = paginationParameters.StartFromId(startFromId);
			}
			IResult<InstaUserShortList> followersList = WaitForResult(() => _instaApi.GetUserFollowersAsync(username, paginationParameters));

			InstaUserShort userShort = followersList.Value.FirstOrDefault();

			return new PaginationUserInfo(new InstaUser(userShort), paginationParameters.NextId);
		}

		public List<string> GetCurrentUserFollowersList(string startFromId, int pageToLoad = 1)
		{
			PaginationParameters paginationParameters = PaginationParameters.MaxPagesToLoad(pageToLoad);
			if (!string.IsNullOrEmpty(startFromId))
			{
				paginationParameters = paginationParameters.StartFromId(startFromId);
			}
			IResult<InstaUserShortList> followersList = WaitForResult(() => _instaApi.GetCurrentUserFollowersAsync( paginationParameters));

			string result = string.Empty;
			followersList.Value.ForEach(u =>
			{
				result += $"{u.UserName};";
			});

			return new List<string>{result, followersList.Value.NextId};
		}

		public List<string> GetUserFollowersList(string username, string startFromId, int pageToLoad = 1)
		{
			PaginationParameters paginationParameters = PaginationParameters.MaxPagesToLoad(pageToLoad);
			if (!string.IsNullOrEmpty(startFromId))
			{
				paginationParameters = paginationParameters.StartFromId(startFromId);
			}
			IResult<InstaUserShortList> followersList = WaitForResult(() => _instaApi.GetUserFollowersAsync(username, paginationParameters));

			string result = string.Empty;
			followersList.Value.ForEach(u =>
			{
				result += $"{u.UserName};";
			});

			return new List<string> { result, followersList.Value.NextId };
		}

		public PaginationUserInfo GetUserFollowers(string startFromId, int pageToLoad = 1)
		{

			PaginationParameters paginationParameters = PaginationParameters.MaxPagesToLoad(pageToLoad);
			if (!string.IsNullOrEmpty(startFromId))
			{
				paginationParameters = paginationParameters.StartFromId(startFromId);
			}
			IResult<InstaUserShortList> followersList = WaitForResult(() => _instaApi.GetCurrentUserFollowersAsync(paginationParameters));

			if (followersList.Value.Count == 0)
			{
				return null;
			}

			InstaUserShort userShort = followersList.Value.FirstOrDefault();
			if (followersList.Value.Count == 1)
			{
				return new PaginationUserInfo(new InstaUser(userShort), followersList.Value.NextId);
			}

			return new PaginationUserInfo(new InstaUser(userShort), null, followersList.Value[1].UserName);
		}

		public bool LikeAmmountOfMedia(string username, int ammountToLike = 1, int pageToLoad = 1, string startFromId = "")
		{
			int currentLikeCounter = 0;
			bool result = false;
			PaginationParameters paginationParameters = PaginationParameters.MaxPagesToLoad(pageToLoad);

			if (startFromId != string.Empty)
			{
				paginationParameters = paginationParameters.StartFromId(startFromId);
			}

			var mediaList = WaitForResult(() => _instaApi.GetUserMediaAsync(username, paginationParameters));
			foreach (var media in mediaList.Value)
			{
				if (currentLikeCounter == ammountToLike)
				{
					break;
				}
				if (!media.HasLiked)
				{
					currentLikeCounter++;
					_instaApi.LikeMediaAsync(media.InstaIdentifier);
					result = true;
				}
			}

			return result;
		}

		public bool IsFollowedByUser(long userId)
		{
			IResult<InstaFriendshipStatus> instaFriendshipStatus =
				WaitForResult(() => _instaApi.GetFriendshipStatusAsync(userId));

			return instaFriendshipStatus.Value.FollowedBy;
		}

		public bool IsFollowedByUser(string userName)
		{
			IResult<InstaUser> user =
				WaitForResult(() => _instaApi.GetUserAsync(userName));
			if (!user.Succeeded)
			{
				return false;
			}
			IResult<InstaFriendshipStatus> instaFriendshipStatus =
				WaitForResult(() => _instaApi.GetFriendshipStatusAsync(user.Value.Pk));

			return instaFriendshipStatus.Value.FollowedBy;
		}

		public InstaCurrentUser GetCurrentUser()
		{
			IResult<InstaCurrentUser> currentUser =
				WaitForResult(() => _instaApi.GetCurrentUserAsync());

			return currentUser.Value;
		}
		public InstaUser GetUser(string username)
		{
			IResult<InstaUser> foundUser =
				WaitForResult(() => _instaApi.GetUserAsync(username));

			return foundUser.Value;
		}

		public bool IsUserAuthenticated()
		{
			if(_instaApi == null)
			{
				return false;
			}
			
			return _instaApi.IsUserAuthenticated;
		}

		public InstaLocationShortList GetGeoTagsByLatLong(double latitude, double longitude, string query = "square")
		{
			return WaitForResult(() => _instaApi.SearchLocation(latitude, longitude, query)).Value;
		}

		public string GetGeoTagIdsAsString(InstaLocationShortList locationsList)
		{
			string result = string.Empty;
			foreach (var location in locationsList)
			{
				result += $"{location.ExternalId};";
			}

			return result;
		}
		public InstaLocationFeed GetLocationFeed(string locationId, int pageToLoad = 1)
		{
			return WaitForResult(() => _instaApi.GetLocationFeed(long.Parse(locationId), PaginationParameters.MaxPagesToLoad(pageToLoad))).Value;
		}
		public string GetUserPksByLocationAsString(InstaLocationFeed locationsFeed, int maxFeedSize)
		{
			string result = string.Empty;
			int count = 0;
			foreach (var media in locationsFeed.Medias)
			{
				count++;
				if (count > maxFeedSize)
				{
					break;
				}
				result += $"{media.User.Pk};";
			}

			return result;
		}

		public string GetUsernamesByLocationAsString(InstaLocationFeed locationsFeed, int maxFeedSize)
		{
			string result = string.Empty;
			int count = 0;
			foreach (var media in locationsFeed.Medias)
			{
				count++;
				if (count > maxFeedSize)
				{
					break;
				}
				result += $"{media.User.UserName};";
			}

			return result;
		}
		private IResult<T> WaitForResult<T>(Func<Task<IResult<T>>> action)
		{
			Task<IResult<T>> task = action();
			task.Wait();

			return task.Result;
		}
	}
}
