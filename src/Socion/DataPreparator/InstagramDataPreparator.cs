﻿using System.Linq;
using ConstantLibrary;
using RabbitMqLibrary;
using RedisLibrary;
using EntityFramework;

namespace Socion.DataPreparator
{
	public class InstagramDataPreparator
	{
		private readonly SocionContext _db;
		private readonly RabbitMq _rabbitMq;

	public InstagramDataPreparator()
		{
			_db = new SocionContext();
			_rabbitMq = new RabbitMq();
		}

		public void SubscribeByTagDataPreparation(uint promotionTaskId)
		{
			PromotionTask promotionTask =
				_db.PromotionTasks.FirstOrDefault(p => p.PromotionTaskId == promotionTaskId);
			AccountPromotionTask accountPromotionTask =
				_db.AccountPromotionTasks.FirstOrDefault(a => a.TaskId == promotionTaskId);

			if (accountPromotionTask == null || promotionTask == null)
			{
				return;
			}
			
			var tagsRedisKey = $"{Constant.Redis.Prefix.SubscribeByTagTask}{accountPromotionTask.SocialNetworkAccountId}";
			if (!Redis.Instance.Database.StringGet(tagsRedisKey).IsNullOrEmpty
			    || !Redis.Instance.Database.StringGet(tagsRedisKey).HasValue)
			{
				Redis.Instance.Database.StringSet(tagsRedisKey, promotionTask.HashTags);
			}

			string rabbitMessage = $"{accountPromotionTask.SocialNetworkAccountId}{Constant.RabbitMq.Delimiter}{promotionTask.DaysToDelete}";
			_rabbitMq.ExchangeDeclare(Constant.RabbitMq.ExchangeName.SubscribeByTagTaskInst, ExchangeType.Fanout);
			_rabbitMq.PublishToExchange(Constant.RabbitMq.ExchangeName.SubscribeByTagTaskInst, rabbitMessage);
		}

		public void SubscribeByGeoLocationDataPreparation(uint promotionTaskId)
		{
			PromotionTask promotionTask =
				_db.PromotionTasks.FirstOrDefault(p => p.PromotionTaskId == promotionTaskId);
			AccountPromotionTask accountPromotionTask =
				_db.AccountPromotionTasks.FirstOrDefault(a => a.TaskId == promotionTaskId);

			if (accountPromotionTask == null || promotionTask == null)
			{
				return;
			}

			var locationsRedisKey = $"{Constant.Redis.Prefix.SubscribtionByLocationsInst}{accountPromotionTask.SocialNetworkAccountId}";
			if (!Redis.Instance.Database.StringGet(locationsRedisKey).IsNullOrEmpty
			    || !Redis.Instance.Database.StringGet(locationsRedisKey).HasValue)
			{
				Redis.Instance.Database.StringSet(locationsRedisKey, promotionTask.Locations);
			}

			string rabbitMessage = $"{accountPromotionTask.SocialNetworkAccountId}{Constant.RabbitMq.Delimiter}{promotionTask.DaysToDelete}";
			_rabbitMq.ExchangeDeclare(Constant.RabbitMq.ExchangeName.SubscribeByLocationsTaskInst, ExchangeType.Fanout);
			_rabbitMq.PublishToExchange(Constant.RabbitMq.ExchangeName.SubscribeByLocationsTaskInst, rabbitMessage);
		}

		public void SubscribeByCompetitorDataPreparation(uint promotionTaskId)
		{
			PromotionTask promotionTask =
				_db.PromotionTasks.FirstOrDefault(p => p.PromotionTaskId == promotionTaskId);
			AccountPromotionTask accountPromotionTask =
				_db.AccountPromotionTasks.FirstOrDefault(a => a.TaskId == promotionTaskId);

			if (accountPromotionTask == null || promotionTask == null)
			{
				return;
			}
			
		var competitorsRedisKey = $"{Constant.Redis.Prefix.SubscribtionCompetitorInst}{accountPromotionTask.SocialNetworkAccountId}";
			if (string.IsNullOrEmpty(Redis.Instance.Database.StringGet(competitorsRedisKey))
			    || !Redis.Instance.Database.StringGet(competitorsRedisKey).HasValue)
			{
				Redis.Instance.Database.StringSet(competitorsRedisKey, promotionTask.Competitors);
			}

			string rabbitMessage = $"{accountPromotionTask.SocialNetworkAccountId}{Constant.RabbitMq.Delimiter}{promotionTask.DaysToDelete}";
			_rabbitMq.ExchangeDeclare(Constant.RabbitMq.ExchangeName.SubscribeByCompetitorTaskInst, ExchangeType.Fanout);
			_rabbitMq.PublishToExchange(Constant.RabbitMq.ExchangeName.SubscribeByCompetitorTaskInst, rabbitMessage);
		}

		public void LikeFollowersDataPreparation(uint promotionTaskId)
		{
			PromotionTask promotionTask =
				_db.PromotionTasks.FirstOrDefault(p => p.PromotionTaskId == promotionTaskId);
			AccountPromotionTask accountPromotionTask =
				_db.AccountPromotionTasks.FirstOrDefault(a => a.TaskId == promotionTaskId);

			if (accountPromotionTask == null || promotionTask == null)
			{
				return;
			}
			
			string rabbitMessage = $"{accountPromotionTask.SocialNetworkAccountId}{Constant.RabbitMq.Delimiter}{promotionTask.LikeAmmount}";
			_rabbitMq.ExchangeDeclare(Constant.RabbitMq.ExchangeName.LikeFollowersTaskInst, ExchangeType.Fanout);
			_rabbitMq.PublishToExchange(Constant.RabbitMq.ExchangeName.LikeFollowersTaskInst, rabbitMessage);
		}

		public void LikeByTagDataPreparation(uint promotionTaskId)
		{
			PromotionTask promotionTask =
				_db.PromotionTasks.FirstOrDefault(p => p.PromotionTaskId == promotionTaskId);
			AccountPromotionTask accountPromotionTask =
				_db.AccountPromotionTasks.FirstOrDefault(a => a.TaskId == promotionTaskId);

			if (accountPromotionTask == null || promotionTask == null)
			{
				return;
			}

			var tagsRedisKey = $"{Constant.Redis.Prefix.LikeByTageInst}{accountPromotionTask.SocialNetworkAccountId}";
			if (string.IsNullOrEmpty(Redis.Instance.Database.StringGet(tagsRedisKey))
			    || !Redis.Instance.Database.StringGet(tagsRedisKey).HasValue)
			{
				Redis.Instance.Database.StringSet(tagsRedisKey, promotionTask.HashTags);
			}

			string rabbitMessage = $"{accountPromotionTask.SocialNetworkAccountId}{Constant.RabbitMq.Delimiter}{promotionTask.LikeAmmount}";
			_rabbitMq.ExchangeDeclare(Constant.RabbitMq.ExchangeName.LikeByTagTaskInst, ExchangeType.Fanout);
			_rabbitMq.PublishToExchange(Constant.RabbitMq.ExchangeName.LikeByTagTaskInst, rabbitMessage);
		}
		public void LikeByGeoLocationDataPreparation(uint promotionTaskId)
		{
			PromotionTask promotionTask =
				_db.PromotionTasks.FirstOrDefault(p => p.PromotionTaskId == promotionTaskId);
			AccountPromotionTask accountPromotionTask =
				_db.AccountPromotionTasks.FirstOrDefault(a => a.TaskId == promotionTaskId);

			if (accountPromotionTask == null || promotionTask == null)
			{
				return;
			}

			var locationsRedisKey = $"{Constant.Redis.Prefix.LikeByLocationsInst}{accountPromotionTask.SocialNetworkAccountId}";
			if (!Redis.Instance.Database.StringGet(locationsRedisKey).IsNullOrEmpty
			    || !Redis.Instance.Database.StringGet(locationsRedisKey).HasValue)
			{
				Redis.Instance.Database.StringSet(locationsRedisKey, promotionTask.Locations);
			}

			string rabbitMessage = $"{accountPromotionTask.SocialNetworkAccountId}{Constant.RabbitMq.Delimiter}{promotionTask.LikeAmmount}";
			_rabbitMq.ExchangeDeclare(Constant.RabbitMq.ExchangeName.LikeByLocationsTaskInst, ExchangeType.Fanout);
			_rabbitMq.PublishToExchange(Constant.RabbitMq.ExchangeName.LikeByLocationsTaskInst, rabbitMessage);
		}


		public void UnsubscribeDataPreparation(long unsubscribeId, int accountId)
		{
			RabbitMq rabbitMq = new RabbitMq();

			string rabbitMessage = $"{unsubscribeId}{Constant.RabbitMq.Delimiter}{accountId}"; 
			_rabbitMq.ExchangeDeclare(Constant.RabbitMq.ExchangeName.UnsubscribeTaskInst, ExchangeType.Fanout);
			rabbitMq.PublishToExchange(Constant.RabbitMq.ExchangeName.UnsubscribeTaskInst, rabbitMessage);
		}
	}
}
