﻿using AutoMapper;
using EntityFramework;
using Hangfire;
using Hangfire.MySql.Core;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using ConstantLibrary;

namespace Socion
{
	public class Startup
	{
		public Startup(IConfiguration configuration)
		{
			Configuration = configuration;
		}

		public IConfiguration Configuration { get; }

		// This method gets called by the runtime. Use this method to add services to the container.
		public void ConfigureServices(IServiceCollection services)
		{
			services.AddDbContext<SocionContext>();
			services.AddHangfire(x => x.UseStorage(new MySqlStorage(Constant.ConnectionString.Hangfire)));
			services.AddSecure();
			services.AddAutoMapper();
			services.AddMvc();
		}

		// This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
		public void Configure(IApplicationBuilder app, IHostingEnvironment env, IdentityContext identityContext)
		{
			if (env.IsDevelopment())
			{
				app.UseDeveloperExceptionPage();
			}
			app.UseAuthentication();

			app.UseHangfireDashboard();
			app.UseHangfireServer();

			app.UseMvc();

			identityContext.Database.EnsureCreated();
		}
	}
}
