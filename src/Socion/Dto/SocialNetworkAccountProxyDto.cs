﻿using System.Runtime.Serialization;

namespace Socion.Dto
{
	[DataContract]
	public class SocialNetworkAccountProxyDto
	{
		[DataMember(Name = ConstantLibrary.Constant.Dto.SocialNetworkAccountProxy.SocialNetworkAccountId.Name)]
		public int SocialNetworkAccountId { get; set; }

		[DataMember(Name = ConstantLibrary.Constant.Dto.SocialNetworkAccountProxy.ProxyId.Name)]
		public int ProxyId { get; set; }
	}
}

