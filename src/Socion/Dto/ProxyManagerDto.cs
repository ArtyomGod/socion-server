﻿using System.Runtime.Serialization;
using ConstantLibrary;

namespace Socion.Dto
{
	[DataContract]
	public class ProxyManagerDto
    {
	    [DataMember(Name = Constant.Dto.ProxyManager.Proxy.Name)]
	    public ProxyDto ProxyDto { get; set; }

	    [DataMember(Name = Constant.Dto.ProxyManager.SocialNetworkAccount.Name)]
		public SocialNetworkAccountDto SocialNetworkAccountDto { get; set; }
	}
}
