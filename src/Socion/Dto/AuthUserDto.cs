﻿using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;
using ConstantLibrary;

namespace Socion.Dto
{
	[DataContract]
	public class AuthUserDto
	{
		[Required]
		[StringLength(Constant.Dto.Shared.Email.MaxLength, MinimumLength = Constant.Dto.Shared.Email.MinLength)]
		[DataMember(Name = Constant.Dto.Shared.Email.Name)]
		public string Email { get; set; }

		[Required]
		[StringLength(Constant.Dto.Shared.Password.MaxLength, MinimumLength = Constant.Dto.Shared.Password.MinLength)]
		[DataMember(Name = Constant.Dto.Shared.Password.Name)]
		public string Password { get; set; }

		[StringLength(Constant.Dto.Shared.Username.MaxLength)]
		[DataMember(Name = Constant.Dto.Shared.Username.Name)]
		public string Username { get; set; }
	}
}
