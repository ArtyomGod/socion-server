﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Socion.Dto
{
    public class UnsubscribeInstTaskDto
    {
		public int AccountDataId { get; set; }
		public int UnfolowingLogin { get; set; }
    }
}
