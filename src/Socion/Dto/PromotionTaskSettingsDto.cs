﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Runtime.Serialization;
using System.Threading.Tasks;
using ConstantLibrary;

namespace Socion.Dto
{
	[DataContract]
	public class PromotionTaskSettingsDto
    {
	    [Required]
	    [DataMember(Name = Constant.Dto.PromotionTaskSettingsDto.PromotionTaskId.Name)]
	    public int PromotionTaskId { get; set; }

	    [Required]
	    [DataMember(Name = Constant.Dto.PromotionTaskSettingsDto.Cron.Name)]
	    public string Cron { get; set; }
	}
}
