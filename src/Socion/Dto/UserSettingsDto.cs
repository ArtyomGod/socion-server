﻿using System.Runtime.Serialization;
using ConstantLibrary;
using System.ComponentModel.DataAnnotations;

namespace Socion.Dto
{
	[DataContract]
	public class UserSettingsDto
	{
		[DataMember(Name = Constant.Dto.Shared.Email.Name)]
		[StringLength(Constant.Dto.Shared.Email.MaxLength, MinimumLength = Constant.Dto.Shared.Email.MinLength)]
		public string Email { get; set; }

		[DataMember(Name = Constant.Dto.UserSettings.NewPassword.Name)]
		[StringLength(Constant.Dto.Shared.Password.MaxLength, MinimumLength = Constant.Dto.Shared.Password.MinLength)]
		public string NewPassword { get; set; }

		[DataMember(Name = Constant.Dto.UserSettings.CurrentPassword.Name)]
		[StringLength(Constant.Dto.Shared.Password.MaxLength, MinimumLength = Constant.Dto.Shared.Password.MinLength)]
		public string CurrentPassword { get; set; }

		[DataMember(Name = Constant.Dto.UserSettings.EmailConfirmed.Name)]
		public bool? EmailConfirmed { get; set; }
	}
}
