﻿using System.Runtime.Serialization;
using ConstantLibrary;

namespace Socion.Dto
{
	[DataContract]
	public class ProxyDto
	{
		[DataMember(Name = Constant.Dto.Proxy.ProxyId.Name)]
		public int ProxyId { get; set; }

		[DataMember(Name = Constant.Dto.Proxy.Value.Name)]
		public string Value { get; set; }

		[DataMember(Name = Constant.Dto.Proxy.IsValid.Name)]
		public bool IsValid { get; set; }

		[DataMember(Name = Constant.Dto.Proxy.Login.Name)]
		public string Login { get; set; }

		[DataMember(Name = Constant.Dto.Proxy.Password.Name)]
		public string Password { get; set; }

		[DataMember(Name = Constant.Dto.Proxy.Type.Name)]
		public ProxyType Type { get; set; }

		[DataMember(Name = Constant.Dto.Proxy.ExpirationDate.Name)]
		public long? ExpirationDate { get; set; }
	}
}
