﻿using ConstantLibrary;
using System.Runtime.Serialization;

namespace Socion.Dto
{
	public interface IPromotionTaskDto
	{
		int PromotionTaskId { get; set; }
		int SocialNetworkAccountId { get; set; }
		PromotionTaskType PromotionTaskType { get; }
	}
}
