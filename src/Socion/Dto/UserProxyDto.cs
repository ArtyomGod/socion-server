﻿using System.Runtime.Serialization;

namespace Socion.Dto
{
	[DataContract]
	public class UserProxyDto
    {
	    [DataMember(Name = ConstantLibrary.Constant.Dto.UserProxy.UserId.Name)]
	    public int UserId { get; set; }

	    [DataMember(Name = ConstantLibrary.Constant.Dto.UserProxy.UserId.Name)]
	    public int ProxyId { get; set; }
	}
}
