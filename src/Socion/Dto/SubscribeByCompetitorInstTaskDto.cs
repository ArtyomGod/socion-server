﻿using System.Runtime.Serialization;
using ConstantLibrary;

namespace Socion.Dto
{
	[DataContract]
	public class SubscribeByCompetitorInstTaskDto : IPromotionTaskDto
	{
		[DataMember(Name = Constant.Dto.SubscribeByCompetitorInstTaskDto.Competitors.Name)]
		public string Competitors { get; set; }

		[DataMember(Name = Constant.Dto.SubscribeByCompetitorInstTaskDto.DaysToDelete.Name)]
		public int DaysToDelete { get; set; }

		[DataMember(Name = Constant.Dto.PromotionTask.PromotionTaskId.Name)]
		[System.ComponentModel.DefaultValue(Constant.Dto.PromotionTask.PromotionTaskId.Default)]
		public int PromotionTaskId { get; set; }

		[DataMember(Name = Constant.Dto.PromotionTask.SocialNetworkAccountId.Name)]
		public int SocialNetworkAccountId { get; set; }

		[DataMember(Name = Constant.Dto.PromotionTask.LikeAmmount.Name)]
		[System.ComponentModel.DefaultValue(Constant.Dto.PromotionTask.LikeAmmount.Default)]
		public int LikeAmmount { get; set; }

		[DataMember(Name = Constant.Dto.PromotionTask.PromotionTaskType.Name)]
		public PromotionTaskType PromotionTaskType => PromotionTaskType.SubscribeByCompetitor;
	}
}
