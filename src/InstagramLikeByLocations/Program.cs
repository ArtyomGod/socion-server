﻿using ConstantLibrary;
using System;
using System.Data.SqlTypes;
using System.Linq;
using EntityFramework;
using Hangfire;
using Hangfire.MySql.Core;
using RabbitMqLibrary;
using Socion.DataPreparator;
using Socion.Helper.SocialNetworkApiHelper.InstagramApiHelper;
using RedisLibrary;

namespace InstagramLikeByLocations
{
	class Program
	{
		static void Main(string[] args)
		{
			GlobalConfiguration.Configuration.UseStorage(new MySqlStorage(Constant.ConnectionString.Hangfire));

			var rabbitMq = new RabbitMq();
			rabbitMq.QueueDeclare();
			rabbitMq.ExchangeDeclare(Constant.RabbitMq.ExchangeName.LikeByLocationsTaskInst, ExchangeType.Fanout);
			rabbitMq.BindQueueToExchange(Constant.RabbitMq.ExchangeName.LikeByLocationsTaskInst);
			rabbitMq.ConsumeQueue(message =>
			{
				var splitedMessage = DelimRabbitMqString(message, 2);
				int accountId = Convert.ToInt32(splitedMessage.First());
				int likeAmmount = Convert.ToInt32(splitedMessage[1]);

				string redisMetricKeyLike = $"{Constant.Redis.Prefix.LikePerDayInst}{accountId}";
				var madeLikeAmmount = Redis.Instance.Database.StringGet(redisMetricKeyLike);
				var maxAmmoutnToLike =
					GetMaximumAllowableValue(Convert.ToInt32(madeLikeAmmount), likeAmmount, Constant.SocialNetworkConstraints.MaxAmmountToLikePerDayInst);
				if (maxAmmoutnToLike <= 0)
				{
					Console.WriteLine($"Account with id: {accountId} reachd Limit Of Liking for today");
					return;
				}

				SocialNetworkAccount socialNetworkAccount;
				Proxy proxy;
				using (var context = new SocionContext())
				{
					socialNetworkAccount = context.SocialNetworkAccounts.FirstOrDefault(account => account.SocialNetworkAccountId == accountId);
					var proxyId = context.SocialNetworkAccountProxies.Where(a => a.SocialNetworkAccountId == socialNetworkAccount.SocialNetworkAccountId)
						.Select(a => a.ProxyId).FirstOrDefault();

					proxy = context.Proxies.FirstOrDefault(a => a.ProxyId == proxyId);
				}
				if (socialNetworkAccount == null)
				{
					Console.WriteLine($"Account with id {accountId} wasn`t found");
					throw new SqlNullValueException();
				}

				IInstagramApiHelper apiHelper = new InstagramApiHelper(socialNetworkAccount, proxy);
				if (!apiHelper.IsUserAuthenticated())
				{
					Console.WriteLine($"Cannot login instagram account {socialNetworkAccount.SocialNetworkAccountId}");
					return;
				}
				string locationsRedisKey = $"{Constant.Redis.Prefix.LikeByLocationsInst}{accountId}";
				string locations = Redis.Instance.Database.StringGet(locationsRedisKey);
				string location = RemoveFirstFollower(ref locations);
				var latLong = location.Split(':');
				Redis.Instance.Database.StringSet(locationsRedisKey, locations);

				string geoIdsRedisKey = $"{Constant.Redis.Prefix.LikeByLocationsGeoIdsInst}{accountId}_{location}";
				string geoIds = Redis.Instance.Database.StringGet(geoIdsRedisKey);
				if (string.IsNullOrEmpty(geoIds))
				{
					var locationShortList = apiHelper.GetGeoTagsByLatLong(Convert.ToDouble(latLong[0]), Convert.ToDouble(latLong[1]));
					geoIds = apiHelper.GetGeoTagIdsAsString(locationShortList);
					Redis.Instance.Database.StringSet(geoIdsRedisKey, geoIds);
				}
				string currGeoId = RemoveFirstFollower(ref geoIds);
				while (string.IsNullOrEmpty(currGeoId) && !string.IsNullOrEmpty(geoIds))
				{
					currGeoId = RemoveFirstFollower(ref geoIds);
				}
				Redis.Instance.Database.StringSet(geoIdsRedisKey, geoIds);

				string toLikeRedisKey = $"{Constant.Redis.Prefix.LikeByLocationsToLikeInst}{accountId}_{currGeoId}";
				string toLikeList = Redis.Instance.Database.StringGet(toLikeRedisKey);
				if (string.IsNullOrEmpty(toLikeList))
				{
					var locationFeed = apiHelper.GetLocationFeed(currGeoId);
					toLikeList = apiHelper.GetUsernamesByLocationAsString(locationFeed);
					Redis.Instance.Database.StringSet(toLikeRedisKey, toLikeList);
				}
				string currUsernameToLike = RemoveFirstFollower(ref toLikeList);
				while (string.IsNullOrEmpty(currUsernameToLike) && !string.IsNullOrEmpty(toLikeList))
				{
					currUsernameToLike = RemoveFirstFollower(ref toLikeList);
				}
				Redis.Instance.Database.StringSet(toLikeRedisKey, toLikeList);				

				apiHelper.LikeAmmountOfMedia(currUsernameToLike, likeAmmount);
				BackgroundJob.Enqueue(() => Redis.Increment(redisMetricKeyLike, likeAmmount));
				Console.WriteLine($"Liked {likeAmmount} times");
			});

			Console.WriteLine("InstagramLikeByLocationsListener has started");
			Console.WriteLine("Press [enter] to exit.");
			Console.ReadKey();
		}

		private static int GetMaximumAllowableValue(int madeLikeAmmount, int ammountToMake, int constraint)
		{
			var newValue = madeLikeAmmount + ammountToMake;
			if (newValue > constraint)
			{
				return constraint - newValue;
			}
			return ammountToMake;
		}

		private static string RemoveFirstFollower(ref string tags)
		{
			string firstTag = string.Empty;
			int tagLastIndex = 0;
			foreach (char ch in tags)
			{
				++tagLastIndex;
				if (ch == ';')
				{
					break;
				}
				firstTag += ch;
			}
			tags = tags.Remove(0, tagLastIndex);

			return firstTag;
		}

		private static string[] DelimRabbitMqString(string input, int ammountOfArguments)
		{
			var splitedMessage = input.Split(Constant.RabbitMq.Delimiter);
			if (splitedMessage.Length != ammountOfArguments)
			{
				throw new ArgumentException();
			}

			return splitedMessage;
		}
	}
}
