﻿using System;
using System.Text;
using Model;
using RabbitMqLibrary;
using RedisLibrary;
using ConstantLibrary;
using MailKit.Net.Smtp;
using MimeKit;
using MimeKit.Text;
using Newtonsoft.Json;

namespace EmailSender
{
	class Program
	{
		static void Main(string[] args)
		{
			var rabbitMq = new RabbitMq();
			rabbitMq.QueueDeclare();
			rabbitMq.ExchangeDeclare(Constant.RabbitMq.ExchangeName.Email, ExchangeType.Fanout);
			rabbitMq.BindQueueToExchange(Constant.RabbitMq.ExchangeName.Email);
			rabbitMq.ConsumeQueue(emailId =>
			{
				string redisKey = $"{Constant.Redis.Prefix.Email}{emailId}";
				string emailInformationString = Redis.Instance.Database.StringGet(redisKey);
				var emailInformation = JsonConvert.DeserializeObject<EmailInformation>(emailInformationString);
				Redis.Instance.Database.KeyDelete(redisKey);

				SendEmail(emailInformation);

				Console.WriteLine("---Email sended---");
				Console.WriteLine(emailInformation.ToString());
				Console.WriteLine("------------------");
			});

			Console.WriteLine("EmailSender has started");
			Console.WriteLine("Press [enter] to exit.");
			Console.ReadKey();
		}
		
		private static void SendEmail(EmailInformation emailInformation)
		{
			var emailMessage = new MimeMessage();
			emailMessage.From.Add(new MailboxAddress(emailInformation.FromName, emailInformation.FromAddress));
			emailMessage.To.Add(new MailboxAddress(emailInformation.ToName, emailInformation.ToAddress));
			emailMessage.Subject = emailInformation.Subject;
			emailMessage.Body = new TextPart(TextFormat.Html)
			{
				Text = emailInformation.Body
			};

			// TODO: Разобраться, есть ли необходимость в переаутентификации для отправки каждого письма
			using (var client = new SmtpClient())
			{
				client.Connect(Constant.EmailSender.Smtp, Constant.EmailSender.SmtpPort, Constant.EmailSender.UseSsl);
				client.Authenticate(Constant.EmailSender.FromAddress, Constant.EmailSender.FromPassword);

				client.Send(emailMessage);
				client.Disconnect(true);
			}
		}
	}
}
